<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ProfilController;
use App\Http\Controllers\Event\EmailBlasting;
use App\Http\Controllers\Event\RekapDataController;
use App\Http\Controllers\Event\ReuniAAUController;
use App\Http\Controllers\Event\UploadFotoController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::controller(LoginController::class)->group(function () {
    Route::get('/login', 'index')->middleware('guest')->name('login');
    Route::post('/login', 'authenticate')->middleware('guest')->name('authenticate');
    Route::get('/logout', 'logout')->middleware('auth')->name('logout');
});

Route::controller(HomeController::class)->group(function () {
    Route::get('/', 'index')->middleware('guest')->name('home');
    Route::post('/', 'store')->middleware('guest')->name('home.store');
    Route::get('/home', 'dashboard')->middleware('auth')->name('dashboard');
});

Route::controller(ProfilController::class)->group(function () {
    Route::get('/indentitas-pribadi', 'index')->middleware('auth')->name('profil.index');
    Route::post('/indentitas-pribadi', 'store')->middleware('auth')->name('profil.store');
});

Route::controller(ReuniAAUController::class)->group(function () {
    Route::get('/event-reuniaau', 'index')->middleware('auth')->name('reuniaau.index');
    Route::post('/event-reuniaau', 'store')->middleware('auth')->name('reuniaau.store');
});

Route::controller(RekapDataController::class)->group(function () {
    Route::get('/rekap-data', 'index')->middleware('auth')->name('rekap.index');
    Route::get('/form-kehadiran', 'kehadiran')->middleware('auth')->name('form_kehadiran.index');
});

Route::controller(UploadFotoController::class)->group(function () {
    Route::get('/upload-gallery', 'index')->middleware('auth')->name('gallery.index');
    Route::post('/upload-gallery', 'store')->middleware('auth')->name('gallery.store');
    Route::delete('/upload-gallery', 'destroy')->middleware('auth')->name('gallery.destroy');
});

Route::controller(EmailBlasting::class)->group(function () {
    Route::get('/email-blasting', 'index')->middleware('auth')->name('blasting.index');
    Route::post('/email-blasting', 'store')->middleware('auth')->name('blasting.store');
});
