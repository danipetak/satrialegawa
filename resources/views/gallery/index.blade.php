@extends('layouts.app')

@section('title', 'Upload Gallery Kenangan')

@section('footer')
<script>
var y = 1;
function addRow(){
    var row = '';
    row +=  '<div class="border-bottom pb-2 mb-3 row-'+(y)+'">' ;
    row +=  '    <div class="row">' ;
    row +=  '        <div class="col"><input type="file" name="gallery[]"></div>' ;
    row +=  '        <div class="col-auto"><button type="button" onclick="deleteRow('+(y)+')" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash"></i></button></div>' ;
    row +=  '    </div>' ;
    row +=  '</div>' ;

    $('.data-loop').append(row);
    y++;
}

function deleteRow(rowid){
    $('.row-'+rowid).remove();
}
</script>

<script>
$("#uploadFile").on('click', function() {
    $(this).attr('style', 'display: none') ;
    $("#loading_data").attr('style', 'display: block') ;
})
</script>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <b>Upload File</b>
    </div>
    <form action="{{ route('gallery.store') }}" enctype="multipart/form-data" method="post">
    @csrf
    <div class="card-body">
        <div class="data-loop">
            <div class="border-bottom pb-2 mb-3">
                <div class="row">
                    <div class="col"><input type="file" name="gallery[]"></div>
                    <div class="col-auto"><button type="button" onclick="addRow()" class="btn btn-outline-success btn-sm"><i class="fa fa-plus"></i></button></div>
                </div>
            </div>
        </div>
        <h5 class="text-center mt-3" style="display: none" id="loading_data"><i class="fas fa-sync-alt fa-spin"></i> Proses Upload File......</h5>
    </div>
    <div class="card-footer text-right">
        <button class="btn btn-primary" type="submit" id="uploadFile">Upload File</button>
    </div>
    </form>
</div>

<div class="card">
    <div class="card-header">
        <b>Daftar Gallery</b>
    </div>
    <div class="card-body">
        <form action="{{ route('gallery.destroy') }}" method="post">
            @csrf @method('delete')
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>File</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $i => $row)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td><a href="{{ url($row->file) }}" target="_blank">{{ $row->nama }}</a></td>
                        <td><button type="submit" name="id" value="{{ $row->id }}" class="btn btn-sm btn-outline-danger"><i class="fa fa-trash"></i></button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </form>
    </div>
</div>
@endsection
