@extends('layouts.homepage')

@section('header')
<style>
    #countdown {
        padding: 0;
        margin: 0;
        font-size: 30px
    }
</style>
@endsection

@section('footer')
<script>
    // Set the date we're counting down to
    var countDownDate = new Date("Nov 26, 2022 08:00:00").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="countdown"
        document.getElementById("count_hari").innerHTML     =   days ;
        document.getElementById("count_jam").innerHTML      =   hours ;
        document.getElementById("count_menit").innerHTML    =   minutes ;
        document.getElementById("count_detik").innerHTML    =   seconds ;

        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("countdown").style      =   "display: none" ;
        }
    }, 1000);
</script>
@endsection

@section('content')
<header class="masthead">
    <div class="container">
        <div class="masthead-subheading">&nbsp;</div>
        <div class="masthead-heading text-uppercase">&nbsp;</div>
        <br><br><br><br><br><br>
        <a class="btn btn-primary btn-xl px-3 py-2 text-uppercase" href="#registrasi">Daftar</a>
        &nbsp;&nbsp;&nbsp;
        <a class="btn btn-outline-primary btn-xl px-3 py-2 text-uppercase" href="{{ route('login') }}">Login</a>
        <br>
        <a class="btn btn-primary py-1 px-3 btn-xl mt-3" href="https://www.instagram.com/legawa20tahun/" aria-label="Instagram">Follow Instagram <span class="btn btn-dark btn-social mx-2"><i class="fab fa-instagram"></i></span></a>
    </div>
</header>
<div class="bg-dark text-light">
    <div class="container text-center py-5">
        <h3>
            Yogyakarta<br>
            26 November 2022 - 08.00 WIB
        </h3>
        <div id="countdown">
            <div class="row mt-4">
                <div class="col mb-2">
                    <div class="bg-secondary rounded p-2">
                        <h3 id="count_hari"></h3>
                        <div>Hari</div>
                    </div>
                </div>
                <div class="col mb-2">
                    <div class="bg-secondary rounded p-2">
                        <h3 id="count_jam"></h3>
                        <div>Jam</div>
                    </div>
                </div>
                <div class="col mb-2">
                    <div class="bg-secondary rounded p-2">
                        <h3 id="count_menit"></h3>
                        <div>Menit</div>
                    </div>
                </div>
                <div class="col mb-2">
                    <div class="bg-secondary rounded p-2">
                        <h3 id="count_detik"></h3>
                        <div>Detik</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="page-section" id="registrasi">
    <div class="container">
        <div class="text-center pb-5">
            <img src="{{ asset('assets/img/sedulur_saklawase.png') }}" style="width: 300px">
            <h2 class="section-heading text-uppercase">Satria Legawa 2002/2022</h2>
            <h2 class="section-heading text-white">ayo melu.....</h2>
        </div>

        <form id="contactForm" action="{{ route('home.store') }}" method="POST">
            @csrf
            <div class="row align-items-stretch mb-5">
                <div class="col-md-6">
                    <div class="form-group">
                        <input autocomplete="off" required class="form-control" id="name" name="name" type="text" placeholder="Nama Lengkap *">
                    </div>
                    <div class="form-group mb-md-0">
                        <input autocomplete="off" required class="form-control" id="password" name="password" type="number" placeholder="NRP *">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input autocomplete="off" required class="form-control" id="email" name="email" type="email" placeholder="E-Mail *">
                    </div>
                    <div class="form-group">
                        <input autocomplete="off" required class="form-control" id="phone" type="number" name="telepon" placeholder="Nomor Telepon *">
                    </div>
                    <div class="form-group mb-md-0">
                        <input autocomplete="off" class="form-control" id="instagram" type="text" name="instagram" placeholder="Akun Instagram">
                    </div>
                </div>
            </div>
            <div class="text-center"><button class="btn btn-primary btn-xl text-uppercase" id="submitButton" type="submit">Daftar</button></div>
        </form>
    </div>
</section>
<!-- Footer-->
<footer class="footer py-4">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4 text-lg-start">Copyright &copy; 2022</div>
            <div class="col-lg-4 my-3 my-lg-0">

            </div>
            <div class="col-lg-4 text-lg-end">
            </div>
        </div>
    </div>
</footer>
@endsection
