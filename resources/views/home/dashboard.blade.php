@extends('layouts.app')

@section('title', 'Halaman Depan')

@section('header')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endsection

@section('footer')
<script src="{{ asset('js/select2.full.js') }}"></script>
@endsection

@section('content')
<div class="row">
    <div class="col-12 col-md">
        <div class="card">
            <div class="card-body p-0">
                <h3 class="p-2 text-center">Data Identitas Pribadi</h3>
                <a href="{{ route('profil.index') }}" class="btn btn-block p-1 rounded-0 btn-primary">Klik Disini</a>
            </div>
        </div>
    </div>
    @if (Auth::user()->ikut)
        @if (Auth::user()->ikut != 'tidak')
        <div class="col-12 col-md">
            <div class="card">
                <div class="card-body p-0">
                    <h3 class="p-2 text-center">Event Reuni AAU</h3>
                    <a href="{{ route('reuniaau.index') }}" class="btn btn-block p-1 rounded-0 btn-primary">Klik Disini</a>
                </div>
            </div>
        </div>
        @endif
    @endif
    <div class="col-12 col-md">
        <div class="card">
            <div class="card-body p-0">
                <h3 class="p-2 text-center">Upload Gallery Kenangan</h3>
                <a href="{{ route('gallery.index') }}" class="btn btn-block p-1 rounded-0 btn-primary">Klik Disini</a>
            </div>
        </div>
    </div>
</div>
@endsection
