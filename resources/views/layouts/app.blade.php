<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon.png') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{ asset('vendor/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/dist/css/adminlte.min.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=DM+Serif+Display&display=swap" rel="stylesheet">
    @yield('header')
        <style>
        #topbar-notification {
            display: none;
            position: fixed;
            z-index: 99999;
            background: #05ab08;
            color: #fff;
            padding: 30px 15px;
            left: 0;
            right: 0;
            top: 0;
            text-align: center;
        }

        #alert-notification {
            display: none;
            position: fixed;
            z-index: 99999;
            background: #e3342f;
            color: #fff;
            padding: 30px 15px;
            left: 0;
            right: 0;
            top: 0;
            text-align: center;
        }
        .cursor{
            cursor: pointer;
        }

        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
        }

        /* Firefox */
        input[type=number] {
        -moz-appearance: textfield;
        }
        </style>
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
        </nav>

        @include('layouts.sidebar')

        <div id="topbar-notification">
            <div class="container">
                <h5><div id="text-notif">My awesome top bar</div></h5>
            </div>
        </div>

        <div id="alert-notification">
            <div class="container">
                <h5><div id="alert-notif">My awesome top bar</div></h5>
            </div>
        </div>

        <div class="content-wrapper">
            <section class="content-header">
                <div class="container-fluid mb-2">
                    <h1>@yield('title')</h1>
                </div>
            </section>

            <section class="content">
                @if (Auth::user()->ikut)
                <div class="alert alert-{{ Auth::user()->ikut == 'tidak' ? 'danger' : (Auth::user()->ikut == 'ikut' ? 'success' : 'info') }} text-center">
                    Anda telah tercatat untuk {{ Auth::user()->ikut == 'tidak' ? 'tidak ikut' : (Auth::user()->ikut == 'ikut' ? 'ikut' : 'mungkin ikut') }} dalam acara Event Reuni AAU
                </div>

                @if (Auth::user()->ikut == 'ikut' || Auth::user()->ikut == 'mungkin')
                    <a href="{{ route('form_kehadiran.index') }}" class="btn btn-warning btn-block mb-3">Unduh Confirmation Letter (Invoice)</a>
                @endif
                @endif

                @yield('content')
            </section>
        </div>

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                {{-- <b>Version</b> 1.1.0 --}}
            </div>
            Copyright &copy; 2022. All rights reserved.
        </footer>

    </div>

    <script src="{{ asset('vendor/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/dist/js/adminlte.min.js') }}"></script>
    @yield('footer')
    <script>
        function showNotif(text) {

            $('#text-notif').html(text);
            $('#topbar-notification').fadeIn();

            setTimeout(function() {
                $('#topbar-notification').fadeOut();
            }, 2000)
        }

        function showAlert(text) {

            $('#alert-notif').html(text);
            $('#alert-notification').fadeIn();

            setTimeout(function() {
                $('#alert-notification').fadeOut();
            }, 2000)
        }
    </script>

    @if(!empty(Session::get('status')) && Session::get('status') == "1")
    <script>
        showNotif("{{Session::get('message')}}");
    </script>
    @endif

    @if(!empty(Session::get('status')) && Session::get('status') == "2")
    <script>
        showAlert("{{Session::get('message')}}");
    </script>
    @endif
</body>
</html>
