<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Satria Legawa</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon.png') }}" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <!-- Google fonts-->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=DM+Serif+Display&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet" />

        <?php if ((time() > strtotime(date('Y-m-d 06:00:00'))) && (time() < strtotime(date('Y-m-d 11:59:59')))) { ?>
            <style>
                header.masthead {
                    background-image: url("../assets/img/header-bg1.jpg");
                }
            </style>
        <?php } else if ((time() > strtotime(date('Y-m-d 12:00:00'))) && (time() < strtotime(date('Y-m-d 17:59:59')))) { ?>
            <style>
            header.masthead {
                background-image: url("../assets/img/header-bg2.jpg");
            }
            </style>
        <?php } else { ?>
            <style>
            header.masthead {
                background-image: url("../assets/img/header-bg.jpg");
            }
            </style>
        <?php } ?>

        <style>
        header.masthead {
            padding-top: 10.5rem;
            padding-bottom: 6rem;
            text-align: center;
            color: #fff;
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-position: center center;
            background-size: cover;
        }

        #topbar-notification {
            display: none;
            position: fixed;
            z-index: 99999;
            background: #05ab08;
            color: #fff;
            padding: 15px;
            left: 0;
            right: 0;
            top: 0;
            text-align: center;
        }

        #alert-notification {
            display: none;
            position: fixed;
            z-index: 99999;
            background: #e3342f;
            color: #fff;
            padding: 15px;
            left: 0;
            right: 0;
            top: 0;
            text-align: center;
        }
        .cursor{
            cursor: pointer;
        }

        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
        }

        /* Firefox */
        input[type=number] {
        -moz-appearance: textfield;
        }
        </style>
        @yield('header')
    </head>
    <body id="page-top">

        <div id="topbar-notification">
            <div class="container">
                <div id="text-notif">My awesome top bar</div>
            </div>
        </div>

        <div id="alert-notification">
            <div class="container">
                <div id="alert-notif">My awesome top bar</div>
            </div>
        </div>

        @yield('content')

        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{ asset('js/scripts.js') }}"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
        @yield('footer')
        <script>
            function showNotif(text) {
                $('#text-notif').html(text);
                $('#topbar-notification').fadeIn();

                setTimeout(function() {
                    $('#topbar-notification').fadeOut();
                }, 5000)
            }

            function showAlert(text) {
                $('#alert-notif').html(text);
                $('#alert-notification').fadeIn();

                setTimeout(function() {
                    $('#alert-notification').fadeOut();
                }, 5000)
            }
        </script>

        @if(!empty(Session::get('status')) && Session::get('status') == "1")
        <script>
            showNotif("{{Session::get('message')}}");
        </script>
        @endif

        @if(!empty(Session::get('status')) && Session::get('status') == "2")
        <script>
            showAlert("{{Session::get('message')}}");
        </script>
        @endif
    </body>
</html>
