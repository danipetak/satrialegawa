<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>@yield('title')</title>

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<link rel="stylesheet" href="{{ asset("vendor/plugins/fontawesome-free/css/all.min.css") }}">
<link rel="stylesheet" href="{{ asset("vendor/plugins/icheck-bootstrap/icheck-bootstrap.min.css") }}">
<link rel="stylesheet" href="{{ asset("vendor/dist/css/adminlte.min.css") }}">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=DM+Serif+Display&display=swap" rel="stylesheet">
<style>
#topbar-notification {
    display: none;
    position: fixed;
    z-index: 99999;
    background: #05ab08;
    color: #fff;
    padding: 15px;
    left: 0;
    right: 0;
    top: 0;
    text-align: center;
}

#alert-notification {
    display: none;
    position: fixed;
    z-index: 99999;
    background: #e3342f;
    color: #fff;
    padding: 15px;
    left: 0;
    right: 0;
    top: 0;
    text-align: center;
}
.cursor{
    cursor: pointer;
}

/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
-webkit-appearance: none;
margin: 0;
}

/* Firefox */
input[type=number] {
-moz-appearance: textfield;
}
</style>

</head>

<div id="topbar-notification">
    <div class="container">
        <div id="text-notif">My awesome top bar</div>
    </div>
</div>

<div id="alert-notification">
    <div class="container">
        <div id="alert-notif">My awesome top bar</div>
    </div>
</div>

<body class="hold-transition login-page">

@yield('content')

<script src="{{ asset("vendor/plugins/jquery/jquery.min.js") }}"></script>
    <script>
        function showNotif(text) {

            $('#text-notif').html(text);
            $('#topbar-notification').fadeIn();

            setTimeout(function() {
                $('#topbar-notification').fadeOut();
            }, 2000)
        }

        function showAlert(text) {

            $('#alert-notif').html(text);
            $('#alert-notification').fadeIn();

            setTimeout(function() {
                $('#alert-notification').fadeOut();
            }, 2000)
        }
    </script>

    @if(!empty(Session::get('status')) && Session::get('status') == "1")
    <script>
        showNotif("{{Session::get('message')}}");
    </script>
    @endif

    @if(!empty(Session::get('status')) && Session::get('status') == "2")
    <script>
        showAlert("{{Session::get('message')}}");
    </script>
    @endif
<script src="{{ asset("vendor/plugins/bootstrap/js/bootstrap.bundle.min.js") }}"></script>
<script src="{{ asset("vendor/dist/js/adminlte.min.js") }}"></script>
</body>
</html>
