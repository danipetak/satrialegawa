<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <span class="brand-link pl-3">
        <span class="brand-text font-weight-light"style="font-family: 'DM Serif Display', serif;">Satria Legawa</span>
    </span>

    <div class="sidebar">
        <div class="user-panel my-2">
            <a href="{{ route('logout') }}" class="float-right mt-1 mr-3"><i class="fa fa-power-off"></i></a>
            <div class="info">
                <span class="d-block">{{ substr(Auth::user()->name, 0, 20) }}</span>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}" class="nav-link {{ request()->routeIs('dashboard') ? 'active' : '' }}">
                        <p>Halaman Depan</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('profil.index') }}" class="nav-link {{ request()->routeIs('profil.index') ? 'active' : '' }}">
                        <p>Identitas Pribadi</p>
                    </a>
                </li>
                @if (Auth::user()->status >= 1)
                <li class="nav-item">
                    <a href="{{ route('rekap.index') }}" class="nav-link {{ request()->routeIs('rekap.index') ? 'active' : '' }}">
                        <p>Rekap Data</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('blasting.index') }}" class="nav-link {{ request()->routeIs('blasting.index') ? 'active' : '' }}">
                        <p>Email Blasting</p>
                    </a>
                </li>
                @endif
                <li class="nav-item">
                    <a href="{{ route('gallery.index') }}" class="nav-link {{ request()->routeIs('gallery.index') ? 'active' : '' }}">
                        <p>Upload Gallery Kenangan</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
