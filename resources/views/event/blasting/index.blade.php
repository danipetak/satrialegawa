@extends('layouts.app')

@section('title', 'Email Blasting')

@section('footer')
<script>
$("#sendToMail").on('click', function() {
    var subject =   $("#subject").val() ;
    var message =   $("#message").val() ;

    if (subject != '' && message != '') {
        $("#sendToMail").attr('style', 'display:none') ;
        $("#loading_data").attr('style', 'display:block') ;
    }
})
</script>
@endsection

@section('content')
<div class="card">
    <form action="{{ route('blasting.store') }}" method="post">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="subject">Subject</label>
                <input type="text" name="subject" autocomplete="off" id="subject" class="form-control" required>
            </div>
    
            <div class="form-group">
                <label for="message">Message</label>
                <textarea name="message" id="message" rows="5" class="form-control" required></textarea>
            </div>
        </div>
        <div class="card-footer text-right">
            <h5 class="text-center mt-3" style="display: none" id="loading_data"><i class="fas fa-sync-alt fa-spin"></i> Loading......</h5>
            <button type="submit" class="btn btn-primary" id="sendToMail">Kirim Email Blasting</button>
        </div>
    </form>
</div>
@endsection