@extends('layouts.app')

@section('title', 'Event Reuni AAU')

@section('header')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endsection

@section('footer')
<script src="{{ asset('js/select2.full.js') }}"></script>

<script>
$("input[name=ikut]").on('click', function() {
    if ($(this).val() == 'ikut') {
        $("#informasi_tidak").attr("style", "display: none") ;
        $("#informasi_mungkin").attr("style", "display: none") ;
        $("#informasi_ikut").attr("style", "display: block") ;
    }
    if ($(this).val() == 'mungkin') {
        $("#informasi_tidak").attr("style", "display: none") ;
        $("#informasi_mungkin").attr("style", "display: block") ;
        $("#informasi_ikut").attr("style", "display: none") ;
    }
    if ($(this).val() == 'tidak') {
        $("#informasi_tidak").attr("style", "display: block") ;
        $("#informasi_mungkin").attr("style", "display: none") ;
        $("#informasi_ikut").attr("style", "display: none") ;
    }
})
</script>

<script>
$("#update_identitas").on("click", function() {
    var ikut                =   $("input[name=ikut]:checked").val() ;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: "{{ route('reuniaau.store') }}",
        method: "POST",
        data: {
            key     :   "ubah_kehadiran" ,
            ikut    :   ikut
        },
        success: function(data) {
            if (data.status == 200) {
                $("#info_submit").attr('style', 'display: block') ;
                $("#form_submit").attr('style', 'display: none') ;
                $("#submit_button").attr("style", "display: none") ;
                document.getElementById("informasi_submit").innerHTML   =   data.msg ;
            } else
            if (data.status == 400) {
                showAlert(data.msg)
            }
            else {
                window.location.href =  "{{ route('reuniaau.index') }}"
            }
        }
    });
})
</script>

<script>
var y = {{ Auth::user()->nama_lain ? (COUNT(json_decode(Auth::user()->nama_lain)) + 1) : 1 }};
function addRowKeluarga(){
    var row = '';
    row +=  '<div class="mt-3 row rowkeluarga-'+(y)+'">' ;
    row +=  '   <div class="col-12 col-md md-2 mb-2 mb-md-0">' ;
    row +=  '       <input type="text" class="nama_lain form-control" placeholder="Tuliskan Nama">' ;
    row +=  '   </div>' ;
    row +=  '   <div class="px-1 col col-md-3 col-lg-2">' ;
    row +=  '   <select class="status_lain form-control">' ;
    row +=  '       <option value="" selected hidden disabled>Pilih Status</option>' ;
    row +=  '       <option value="Orang Tua">Orang Tua</option>' ;
    row +=  '       <option value="Mertua">Mertua</option>' ;
    row +=  '       <option value="Asisten RT">Asisten RT</option>' ;
    row +=  '   </select>' ;
    row +=  '   </div>' ;
    row +=  '   <div class="px-1 col col-md-3 col-lg-2">' ;
    row +=  '       <select class="lain_ikut form-control">' ;
    row +=  '           <option value="" selected hidden disabled>Kehadiran</option>' ;
    row +=  '           <option value="ikut">Ikut</option>' ;
    row +=  '           <option value="mungkin">Mungkin</option>' ;
    row +=  '           <option value="tidak">Tidak</option>' ;
    row +=  '       </select>' ;
    row +=  '   </div>' ;
    row +=  '   <div class="col-auto">' ;
    row +=  '       <button type="button" onclick="deleteRowKeluarga('+(y)+')" class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>' ;
    row +=  '   </div>' ;
    row +=  '</div>' ;

    $('.data-loop-keluarga').append(row);
    $('.select2').select2();
    y++;
}

function deleteRowKeluarga(rowid){
    $('.rowkeluarga-'+rowid).remove();
}
</script>

<script>
var x = {{ Auth::user()->jumlah_tambahan ? (COUNT(json_decode(Auth::user()->jumlah_tambahan)) + 1) : 1 }};;
function addRowTambahan(){
    var row = '';
    row +=  '<div class="mt-3 row rowtambahan-'+(x)+'">' ;
    row +=  '   <div class="col">' ;
    row +=  '       <select class="type_kamar_tambahan form-control">' ;
    row +=  '           <option value="" selected hidden disabled>Pilih Type Kamar Hotel</option>' ;
    row +=  '           <option value="King">King (Single Bed)</option>' ;
    row +=  '           <option value="Twin">Twin (Double Bed)</option>' ;
    row +=  '       </select>' ;
    row +=  '   </div>' ;
    row +=  '   <div class="px-1 col-3 col-md-1">' ;
    row +=  '       <input type="text" class="jumlah_tambahan form-control" placeholder="Jumlah">' ;
    row +=  '   </div>' ;
    row +=  '   <div class="col-auto">' ;
    row +=  '       <button type="button" onclick="deleteRowTambahan('+(x)+')" class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>' ;
    row +=  '   </div>' ;
    row +=  '</div>' ;

    $('.data-loop-tambahan').append(row);
    $('.select2').select2();
    x++;
}

function deleteRowTambahan(rowid){
    $('.rowtambahan-'+rowid).remove();
}
</script>

<script>
$("#form_ubah_tambahan").on('click', function() {
    $("#informasi_tambahan").attr('style', 'display: block') ;
    $("#kamar_tambahan").attr('style', 'display: block') ;
})
</script>
<script>
$("#form_ubah_anggota_tambahan").on('click', function() {
    $("#keluarga_tambahan").attr('style', 'display: block') ;
    $("#keluarga_kolom_tambahan").attr('style', 'display: block') ;
})
</script>

<script>
$("#finish_data").on("click", function() {
    var nama_lain           =   document.getElementsByClassName("nama_lain");
    var namalain            =   [];

    var status_lain         =   document.getElementsByClassName("status_lain");
    var statuslain          =   [];

    var lain_ikut           =   document.getElementsByClassName("lain_ikut");
    var lainikut            =   [];

    var ct_namalain =   0 ;
    for (var i = 0; i < nama_lain.length; ++i) {
        if (nama_lain[i].value) {
            ct_namalain     +=  nama_lain[i].value ? 1 : 0 ;
            namalain.push(nama_lain[i].value);
        }
    }

    var ct_statuslain =   0 ;
    for (var i = 0; i < status_lain.length; ++i) {
        if (status_lain[i].value) {
            ct_statuslain     +=  status_lain[i].value ? 1 : 0 ;
            statuslain.push(status_lain[i].value);
        }
    }

    for (var i = 0; i < nama_lain.length; ++i) {
        if (nama_lain[i].value) {
            if (lain_ikut[i].value) {
                lainikut.push(lain_ikut[i].value);
            } else {
                lainikut.push('null');
            }
        }
    }

    if (ct_namalain != ct_statuslain) {
        showAlert("Anggota Keluarga Tambahan Belum Lengkap")
    } else {
        var type_kamar          =   document.getElementsByClassName("type_kamar_tambahan");
        var typekamar           =   [];

        var jumlah_tambahan     =   document.getElementsByClassName("jumlah_tambahan");
        var jumlahtambahan      =   [];

        var ct_typekamar        =   0 ;
        for (var i = 0; i < type_kamar.length; ++i) {
            if (type_kamar[i].value) {
                ct_typekamar    +=  type_kamar[i].value ? 1 : 0 ;
                typekamar.push(type_kamar[i].value);
            }
        }

        var ct_jumlahtambahan   =   0 ;
        for (var i = 0; i < jumlah_tambahan.length; ++i) {
            if (jumlah_tambahan[i].value) {
                ct_jumlahtambahan   +=  jumlah_tambahan[i].value ? 1 : 0 ;
                jumlahtambahan.push(jumlah_tambahan[i].value);
            }
        }

        if (ct_typekamar != ct_jumlahtambahan) {
            showAlert("Tambahan Kamar Belum Lengkap")
        } else {
            var titik_penjemputan   =   $("#titik_penjemputan").val() ;
            var type_kamar          =   $("#type_kamar").val() ;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "{{ route('reuniaau.store') }}",
                method: "POST",
                data: {
                    namalain            :   namalain ,
                    statuslain          :   statuslain ,
                    lainikut            :   lainikut ,
                    typekamar           :   typekamar ,
                    jumlahtambahan      :   jumlahtambahan ,
                    titik_penjemputan   :   titik_penjemputan ,
                    type_kamar          :   type_kamar
                },
                success: function(data) {
                    $('#InformasiDone').modal('show');
                }
            });
        }
    }
})
</script>
@endsection

@section('content')

{{-- <form action="{{ route('reuniaau.store') }}" method="post"> --}}
    @csrf
    <div class="card">
        <div class="card-body">
            <div class="form-group">
                <label for="keluarga_lain">Anggota Keluarga Tambahan</label>
                <div class="data-loop-keluarga">
                    @if (Auth::user()->nama_lain)
                        @php
                            $status_lain    =   json_decode(Auth::user()->status_lain) ;
                            $lain_ikut      =   json_decode(Auth::user()->lain_ikut) ;
                        @endphp
                        @foreach (json_decode(Auth::user()->nama_lain) as $i => $row)
                        <div class="row rowkeluarga-{{ ($i + 1) }} mb-3">
                            <div class="col-12 col-md md-2 mb-2 mb-md-0">
                                <input type="text" class="nama_lain form-control" value="{{ $row }}" placeholder="Tuliskan Nama">
                            </div>
                            <div class="px-1 col col-md-3 col-lg-2">
                                <select class="status_lain form-control">
                                    <option value="" selected hidden disabled>Pilih Status</option>
                                    <option value="Orang Tua" {{ $status_lain[$i] == "Orang Tua" ? "selected" : "" }}>Orang Tua</option>
                                    <option value="Mertua" {{ $status_lain[$i] == "Mertua" ? "selected" : "" }}>Mertua</option>
                                    <option value="Asisten RT" {{ $status_lain[$i] == "Asisten RT" ? "selected" : "" }}>Asisten RT</option>
                                </select>
                            </div>
                            <div class="px-1 col col-md-3 col-lg-2">
                                <select class="lain_ikut form-control">
                                    <option value="" selected hidden disabled>Kehadiran</option>
                                    <option value="ikut" {{ $lain_ikut ? ($lain_ikut[$i] == "ikut" ? "selected" : "") : "" }}>Ikut</option>
                                    <option value="mungkin" {{ $lain_ikut ? ($lain_ikut[$i] == "mungkin" ? "selected" : "") : "" }}>Mungkin</option>
                                    <option value="tidak" {{ $lain_ikut ? ($lain_ikut[$i] == "tidak" ? "selected" : "") : "" }}>Tidak</option>
                                </select>
                            </div>
                            <div class="col-auto">
                                <button type="button" onclick="deleteRowKeluarga({{ ($i + 1) }})" class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>
                            </div>
                        </div>
                        @endforeach
                        <div class="row">
                            <div class="col-12 col-md md-2 mb-2 mb-md-0">
                                <input type="text" class="nama_lain form-control" placeholder="Tuliskan Nama">
                            </div>
                            <div class="px-1 col col-md-3 col-lg-2">
                                <select class="status_lain form-control">
                                    <option value="" selected hidden disabled>Pilih Status</option>
                                    <option value="Orang Tua">Orang Tua</option>
                                    <option value="Mertua">Mertua</option>
                                    <option value="Asisten RT">Asisten RT</option>
                                </select>
                            </div>
                            <div class="px-1 col col-md-3 col-lg-2">
                                <select class="lain_ikut form-control">
                                    <option value="" selected hidden disabled>Kehadiran</option>
                                    <option value="ikut">Ikut</option>
                                    <option value="mungkin">Mungkin</option>
                                    <option value="tidak">Tidak</option>
                                </select>
                            </div>
                            <div class="col-auto">
                                <button type="button" onclick="addRowKeluarga()" class="btn btn-outline-success"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    @else
                    <div class="row">
                        <div class="col-12 col-md md-2 mb-2 mb-md-0">
                            <input type="text" class="nama_lain form-control" placeholder="Tuliskan Nama">
                        </div>
                        <div class="px-1 col col-md-3 col-lg-2">
                            <select class="status_lain form-control">
                                <option value="" selected hidden disabled>Pilih Status</option>
                                <option value="Orang Tua">Orang Tua</option>
                                <option value="Mertua">Mertua</option>
                                <option value="Asisten RT">Asisten RT</option>
                            </select>
                        </div>
                        <div class="px-1 col col-md-3 col-lg-2">
                            <select class="lain_ikut form-control">
                                <option value="" selected hidden disabled>Kehadiran</option>
                                <option value="ikut">Ikut</option>
                                <option value="mungkin">Mungkin</option>
                                <option value="tidak">Tidak</option>
                            </select>
                        </div>
                        <div class="col-auto">
                            <button type="button" onclick="addRowKeluarga()" class="btn btn-outline-success"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="">Titik Penjemputan</label>
                <select name="titik_penjemputan" id="titik_penjemputan" data-width="100%" data-placeholder="Pilih Titik Penjemputan" required class="form-control select2">
                    <option value=""></option>
                    <option value="Tidak Dijemput" {{ Auth::user()->titik_penjemputan == "Tidak Dijemput" ? "selected" : "" }}>Tidak Dijemput / Kendaraan Pribadi</option>
                    <option value="Adi Sucipto Airport 08" {{ Auth::user()->titik_penjemputan == "Adi Sucipto Airport 08" ? "selected" : "" }}>Adi Sucipto Airport (08.00 WIB)</option>
                    <option value="Adi Sucipto Airport 10" {{ Auth::user()->titik_penjemputan == "Adi Sucipto Airport 10" ? "selected" : "" }}>Adi Sucipto Airport (10.00 WIB)</option>
                    <option value="Yogyakarta International Airport 09" {{ Auth::user()->titik_penjemputan == "Yogyakarta International Airport 09" ? "selected" : "" }}>Yogyakarta International Airport (09.00 WIB)</option>
                    <option value="Stasiun Tugu 08" {{ Auth::user()->titik_penjemputan == "Stasiun Tugu 08" ? "selected" : "" }}>Stasiun Tugu (08.00 WIB)</option>
                    <option value="Stasiun Tugu 10" {{ Auth::user()->titik_penjemputan == "Stasiun Tugu 10" ? "selected" : "" }}>Stasiun Tugu (10.00 WIB)</option>
                </select>
            </div>

            <div class="form-group">
                <label for="">Type Kamar Hotel (Yang Disediakan Panitia)</label>
                <select name="type_kamar" id="type_kamar" data-width="100%" data-placeholder="Pilih Type Kamar Hotel" required class="form-control">
                    <option value="" selected hidden disabled>Pilih Type Kamar Hotel</option>
                    <option value="King" {{ Auth::user()->type_kamar == "King" ? "selected" : "" }}>King (Single Bed)</option>
                    <option value="Twin" {{ Auth::user()->type_kamar == "Twin" ? "selected" : "" }}>Twin (Double Bed)</option>
                </select>
            </div>

            <div class="form-group">
                <label for="">Tambahan Kamar (Biaya Pribadi)</label>
                <div class="data-loop-tambahan" id="kamar_tambahan">
                    @if (Auth::user()->type_kamar_tambahan)
                    @php
                        $jumlah_tambahan    =   json_decode(Auth::user()->jumlah_tambahan) ;
                    @endphp
                    @foreach (json_decode(Auth::user()->type_kamar_tambahan) as $i => $row)
                    <div class="row rowtambahan-{{ ($i + 1) }} mb-3">
                        <div class="col">
                            <select class="type_kamar_tambahan form-control">
                                <option value="King" {{ $row == "King" ? "selected" : "" }}>King (Single Bed)</option>
                                <option value="Twin" {{ $row == "Twin" ? "selected" : "" }}>Twin (Double Bed)</option>
                            </select>
                        </div>
                        <div class="px-1 col-3 col-md-1">
                            <input type="text" class="jumlah_tambahan form-control" value="{{ $jumlah_tambahan[$i] }}" placeholder="Jumlah">
                        </div>
                        <div class="col-auto">
                            <button type="button" onclick="deleteRowTambahan({{ ($i + 1) }})" class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    @endforeach
                    <div class="row">
                        <div class="col">
                            <select class="type_kamar_tambahan form-control">
                                <option value="" selected hidden disabled>Pilih Type Kamar Hotel</option>
                                <option value="King">King (Single Bed)</option>
                                <option value="Twin">Twin (Double Bed)</option>
                            </select>
                        </div>
                        <div class="px-1 col-3 col-md-1">
                            <input type="text" class="jumlah_tambahan form-control" placeholder="Jumlah">
                        </div>
                        <div class="col-auto">
                            <button type="button" onclick="addRowTambahan()" class="btn btn-outline-success"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        <div class="col">
                            <select class="type_kamar_tambahan form-control">
                                <option value="" selected hidden disabled>Pilih Type Kamar Hotel</option>
                                <option value="King">King (Single Bed)</option>
                                <option value="Twin">Twin (Double Bed)</option>
                            </select>
                        </div>
                        <div class="px-1 col-3 col-md-1">
                            <input type="text" class="jumlah_tambahan form-control" placeholder="Jumlah">
                        </div>
                        <div class="col-auto">
                            <button type="button" onclick="addRowTambahan()" class="btn btn-outline-success"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    @endif
                </div>
            </div>

        </div>
        <div class="card-footer text-right">
            <button type="button" class="btn btn-outline-primary mr-3" data-toggle="modal" data-target="#perubahanHadir">Perubahan Kehadiran</button>
            <button class="btn btn-primary" id="finish_data" type="button">Submit</button>
        </div>
    </div>
{{-- </form> --}}

<div class="modal fade" id="InformasiDone" tabindex="-1" aria-labelledby="InformasiDoneLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">

                <div class="text-center">
                    <div class="alert alert-success">
                        Anda all set, akan ada pemberitahuan lebih lanjut. Update kembali jika ada perubahan
                    </div>
                    <a class="btn btn-outline-danger mb-3 mb-sm-0" href="{{ route('dashboard') }}">Kembali Ke Halaman Depan</a>&nbsp;
                    <a class="btn btn-primary" href="{{ route('reuniaau.index') }}">Tetap di Halaman Ini</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="perubahanHadir" tabindex="-1" aria-labelledby="perubahanHadirLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div id="form_submit">
                    <h5 class="text-center mt-3 mb-4">Ikut Reuni?</h5>
                    <div class="row">
                        <div class="col text-center">
                            <input type="radio" name="ikut" id="ikut" {{ Auth::user()->ikut == 'ikut' ? "checked" : ''}} value="ikut">
                            <label for="ikut">Ikut</label>
                        </div>
                        <div class="col text-center">
                            <input type="radio" name="ikut" id="mungkin" {{ Auth::user()->ikut == 'mungkin' ? "checked" : ''}} value="mungkin">
                            <label for="mungkin">Mungkin</label>
                        </div>
                        <div class="col text-center">
                            <input type="radio" name="ikut" id="tidak" {{ Auth::user()->ikut == 'tidak' ? "checked" : ''}} value="tidak">
                            <label for="tidak">Tidak</label>
                        </div>
                    </div>
                    <div class="text-center" id="informasi_ikut" style="{{ Auth::user()->ikut == "ikut" ? "" : "display: none" }}">
                        Sak taeek!!!<br>
                        Acarane mesti gayeng
                    </div>
                    <div class="text-center" id="informasi_mungkin" style="{{ Auth::user()->ikut == "mungkin" ? "" : "display: none" }}">
                        Sing penting yakin!!!<br>
                        Gak ada lo gak rame...
                    </div>
                    <div class="text-center" id="informasi_tidak" style="{{ Auth::user()->ikut == "tidak" ? "" : "display: none" }}">
                        Asuuu og...!!!!!<br>
                        Mbok melu.... Mesakke panitia sing nyiapke
                    </div>
                </div>
                <div id="info_submit" class="text-center" style="display: none">
                    <div class="alert alert-info" id="informasi_submit"></div>
                    <a class="btn btn-outline-danger mb-3 mb-sm-0" href="{{ route('dashboard') }}">Kembali Ke Halaman Depan</a>&nbsp;
                    <a class="btn btn-primary" href="{{ route('reuniaau.index') }}">Tetap di Halaman Ini</a>
                </div>
            </div>
            <div class="modal-footer" id="submit_button">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="update_identitas" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>
</div>
@endsection
