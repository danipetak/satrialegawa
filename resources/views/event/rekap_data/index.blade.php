@extends('layouts.app')

@section('title', "Rekap Data")

@section('footer')
<script>
    $("#data_view").load("{{ route('rekap.index', ['key' => 'view']) }}") ;

    $("#cari").on("keyup", function() {
        var cari        =   encodeURIComponent($("#cari").val()) ;
        var kehadiran   =   $("#kehadiran").val() ;
        $("#data_view").load("{{ route('rekap.index', ['key' => 'view']) }}&search=" + cari + "&kehadiran=" + kehadiran) ;
    })

    $("#kehadiran").on("change", function() {
        var cari        =   encodeURIComponent($("#cari").val()) ;
        var kehadiran   =   $("#kehadiran").val() ;
        $("#data_view").load("{{ route('rekap.index', ['key' => 'view']) }}&search=" + cari + "&kehadiran=" + kehadiran) ;
    })
</script>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <a href="{{ route('rekap.index', ['key' => 'unduh']) }}" class="btn btn-sm btn-success py-0 float-right">Unduh Excel</a>
        <b>Daftar Data Masuk</b>
    </div>
    <div class="card-body">
        <div class="row mb-4">
            <div class="col">
                <div class="bg-secondary px-2">Jumlah Peserta Hadir</div>
                <div class="border p-2">
                    {{ $peserta }}
                </div>
            </div>
            <div class="col">
                <div class="bg-secondary px-2">Hadir</div>
                <div class="border p-2">
                    {{ $hadir }}
                </div>
            </div>
            <div class="col">
                <div class="bg-secondary px-2">Mungkin</div>
                <div class="border p-2">
                    {{ $mungkin }}
                </div>
            </div>
            <div class="col">
                <div class="bg-secondary px-2">Tidak</div>
                <div class="border p-2">
                    {{ $tidak }}
                </div>
            </div>
            <div class="col">
                <div class="bg-secondary px-2">Kamar King</div>
                <div class="border p-2">
                    {{ $king }}
                </div>
            </div>
            <div class="col">
                <div class="bg-secondary px-2">Kamar Twin</div>
                <div class="border p-2">
                    {{ $twin }}
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-12 col-md">
                <input type="text" placeholder="Cari..." id="cari" class="form-control mb-3">
            </div>
            <div class="col-12 col-md-auto">
                <select id="kehadiran" class="form-control">
                    <option value="all">Tampilkan Semua</option>
                    <option value="ikut">Ikut</option>
                    <option value="mungkin">Mungkin</option>
                    <option value="tidak">Tidak</option>
                    <option value="belumisi">Belum Mengisi</option>
                </select>
            </div>
        </div>
        <div id="data_view"></div>
    </div>
</div>
@endsection
