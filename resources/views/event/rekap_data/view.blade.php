<div class="table-responsive">
    <table class="table table-sm table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>NRP</th>
                <th>Email</th>
                <th>Nomor Telepon</th>
                <th>Pangkat</th>
                <th>Korp</th>
                <th>Kehadiran</th>
                <th>Ukuran Kaos</th>
                <th>Nama Istri</th>
                <th>Ukuran Kaos Istri</th>
                <th>Anak</th>
                <th>Umur Anak</th>
                <th>Keluarga Tambahan</th>
                <th>Titik Jemput</th>
                <th>Type Kamar</th>
                <th>Tambahan Kamar</th>
                <th>Hadir</th>
                <th>Mungkin</th>
                <th>Tidak</th>
                <th>Total</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $x => $row)
            @php
            $hadir      =   0 ;
            $mungkin    =   0 ;
            $tidak      =   0 ;

            $hadir      +=  $row->ikut  ==  'ikut' ? 1 : 0 ;
            $mungkin    +=  $row->ikut  ==  'mungkin' ? 1 : 0 ;
            $tidak      +=  $row->ikut  ==  'tidak' ? 1 : 0 ;

            if ($row->anak_ikut) {
                $anak_hadir =   json_decode($row->anak_ikut) ;
                for ($i=0; $i < COUNT($anak_hadir); $i++) {
                    if ($anak_hadir[$i] == 'ikut') {
                        $hadir      +=   1 ;
                    }
                    if ($anak_hadir[$i] == 'mungkin') {
                        $mungkin    +=   1 ;
                    }
                    if ($anak_hadir[$i] == 'null') {
                        $tidak      +=   1 ;
                    }
                }
            }

            if ($row->lain_ikut) {
                $lain_hadir =   json_decode($row->lain_ikut) ;
                for ($i=0; $i < COUNT($lain_hadir); $i++) {
                    if ($lain_hadir[$i] == 'ikut') {
                        $hadir      +=   1 ;
                    }
                    if ($lain_hadir[$i] == 'mungkin') {
                        $mungkin    +=   1 ;
                    }
                    if ($lain_hadir[$i] == 'null') {
                        $tidak      +=   1 ;
                    }
                }
            }

            if ($row->istri_hadir) {
                if ($row->istri_hadir == 'ikut') {
                    $hadir      +=   1 ;
                }
                if ($row->istri_hadir == 'mungkin') {
                    $mungkin    +=   1 ;
                }
                if ($row->istri_hadir == 'null') {
                    $tidak      +=   1 ;
                }
                if ($row->istri_hadir == 'tidak') {
                    $tidak      +=   1 ;
                }
            }
            @endphp
            <tr>
                <td>{{ ++$x }}</td>
                <td>{{ $row->name }}</td>
                <td>{{ $row->nrp }}</td>
                <td>{{ $row->email }}</td>
                <td>{{ $row->telepon }}</td>
                <td>{{ $row->pangkat }}</td>
                <td>{{ $row->korp }}</td>
                <td>{{ $row->ikut }}</td>
                <td>{{ $row->ukuran_kaos_peserta }}</td>
                <td>{{ $row->istri }} {{ $row->istri_hadir ? "(" . $row->istri_hadir . ")" : "" }}</td>
                <td>{{ $row->ukuran_kaos_istri }}</td>
                <td>
                    <ul class="m-0 pl-4">
                        @if ($row->anak)
                        @php
                            $umur_anak  =   json_decode($row->umur) ;
                            $anak_ikut  =   json_decode($row->anak_ikut) ;
                            $jumlah_ank =   0 ;
                        @endphp
                        @foreach (json_decode($row->anak) as $i => $item)
                        @if ($item != null)
                        @php
                            $jumlah_ank +=  1 ;
                        @endphp
                        <li>{{ $item }} {{ $anak_ikut ? ($anak_ikut[$i] == 'null' ? '' : $anak_ikut[$i] ?? '') : '' }}</li>
                        @endif
                        @endforeach
                        @endif
                    </ul>
                </td>
                <td>
                    <ul class="m-0 pl-4">
                        @for ($x = 0; $x < $jumlah_ank; $x++)
                        <li>{{ $umur_anak[$x] }}</li>
                        @endfor
                    </ul>
                </td>
                <td>
                    <ul class="m-0 pl-4">
                        @if ($row->nama_lain)
                        @php
                            $status  =   json_decode($row->status_lain) ;
                        @endphp
                        @foreach (json_decode($row->nama_lain) as $i => $item)
                        @if ($item != null)
                        <li>{{ $item }} ({{ $status[$i] }})</li>
                        @endif
                        @endforeach
                        @endif
                    </ul>
                </td>
                <td>{{ $row->titik_penjemputan }}</td>
                <td>{{ $row->type_kamar }}</td>
                <td>
                    <ul class="m-0 pl-4">
                        @if ($row->type_kamar_tambahan)
                        @php
                            $tambah  =   json_decode($row->jumlah_tambahan) ;
                        @endphp
                        @foreach (json_decode($row->type_kamar_tambahan) as $i => $item)
                        @if ($item != null)
                        <li>{{ $item }} ({{ $tambah[$i] }})</li>
                        @endif
                        @endforeach
                        @endif
                    </ul>
                </td>
                <td>{{ $hadir }}</td>
                <td>{{ $mungkin }}</td>
                <td>{{ $tidak }}</td>
                <td>{{ ($hadir + $mungkin + $tidak) }}</td>
                <td>
                    <div style="width: 110px"></div>
                    <a class="btn btn-outline-info btn-sm mr-2" href="{{ route('form_kehadiran.index', ['id' => $row->id]) }}"><i class="fa fa-file-pdf"></i></a>
                    <button class="btn btn-outline-primary btn-sm mr-2" data-toggle="modal" data-target="#editData{{ $row->id }}"><i class="fa fa-file"></i></button>
                    <button class="btn btn-outline-danger btn-sm edit_data" data-id="{{ $row->id }}"><i class="fa fa-trash"></i></button>
                </td>
            </tr>

            <div class="modal fade" id="editData{{ $row->id }}" tabindex="-1" aria-labelledby="editData{{ $row->id }}Label" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editData{{ $row->id }}Label">Edit Data</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="nama{{ $row->id }}">Nama</label>
                                <input type="text" id="nama{{ $row->id }}" value="{{ $row->name }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="nrp{{ $row->id }}">NRP</label>
                                <input type="number" id="nrp{{ $row->id }}" value="{{ $row->nrp }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="email{{ $row->id }}">Email</label>
                                <input type="email" id="email{{ $row->id }}" value="{{ $row->email }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="nomor{{ $row->id }}">Nomor Telepon</label>
                                <input type="number" id="nomor{{ $row->id }}" value="{{ $row->telepon }}" class="form-control">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary update_data" data-id="{{ $row->id }}">Update</button>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </tbody>
    </table>
</div>

<script>
$(".update_data").on('click', function() {
    var id      =   $(this).data('id') ;
    var nama    =   $("#nama" + id).val() ;
    var nrp     =   $("#nrp" + id).val() ;
    var email   =   $("#email" + id).val() ;
    var nomor   =   $("#nomor" + id).val() ;
    var cari    =   encodeURIComponent($("#cari").val()) ;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: "{{ route('profil.store') }}",
        method: "POST",
        data: {
            key     :   'update_data' ,
            id      :   id ,
            nama    :   nama ,
            nrp     :   nrp ,
            email   :   email ,
            nomor   :   nomor
        },
        success: function(data) {
            showNotif(data.msg) ;
            $('.modal-backdrop').remove();
            $('body').removeClass('modal-open');
            $('#editData' + id).modal('hide');
            $("#data_view").load("{{ route('rekap.index', ['key' => 'view']) }}&search=" + cari) ;
        }
    });
})
</script>

<script>
$(".edit_data").on('click', function() {
    var id      =   $(this).data('id') ;
    var cari    =   encodeURIComponent($("#cari").val()) ;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: "{{ route('profil.store') }}",
        method: "POST",
        data: {
            key     :   'hapus_data' ,
            id      :   id
        },
        success: function(data) {
            showNotif(data.msg) ;
            $("#data_view").load("{{ route('rekap.index', ['key' => 'view']) }}&search=" + cari) ;
        }
    });
})
</script>
