<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $data->name }}</title>
    {{-- <link href="https://fonts.googleapis.com/css2?family=Audiowide&display=swap" rel="stylesheet"> --}}
    <style>
        body {
            /* font-family: 'Audiowide', cursive; */
            font-family: sans-serif;
            color: #484848;
            padding: 0 30px
        }

        thead th {
            padding: 10px 5px;
            background-color: #e29c9c
        }
        tbody td {
            padding: 7px;
        }

        tbody tr {
            border-bottom: 1px solid #e29c9c
        }

        #border_list {
            position: absolute;
            left: -46px;
            top: -46px ;
            background-color: #e1e29c;
            width: 50px;
            height: 100%;

        }
    </style>
</head>
<body>
    <div id="border_list"></div>
    <img src="satrialegawa.jpg" style="height: 90px; float: right">
    <div>
        <h2 style="margin: 0; padding: 0">2002 / 2022 Dwi Dasawarsa<br>SATRIA LEGAWA</h2>
        <img style="opacity: 1; height: 25px" src="sedulur_saklawase_dark.png">
    </div>
    <br><br><br>

    <div style="margin-bottom : 15px; padding-bottom: 10px">
        <div style="float: right; text-align: right">
            <div>{{ $data->email }}</div>
            <div>({{ $data->telepon }})</div>
            <div style="margin-top: 10px"><small>Data Update Terakhir :</small><br>{{ $data->updated_at }}</div>
        </div>

        <b>DATA PESERTA</b>
        <div>{{ $data->name }} ({{ $data->nrp }})</div>
        <div>{{ $data->pangkat }} {{ $data->korp }}</div>
        <div>{{ $data->tanggal_lahir }}</div>
    </div>

    @php
        $hadir  =   0 ;
        $hadir  +=  $data->ikut == 'ikut' ? 1 : 0 ;
        $hadir  +=  $data->istri_hadir == 'ikut' ? 1 : 0 ;
    @endphp

    <table style="border-collapse: collapse; width: 100%; margin-bottom: 15px">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Status</th>
                <th>Ukuran Kaos</th>
                <th>Kehadiran</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $data->name }}</td>
                <td>Peserta</td>
                <td style="text-align: center">{{ $data->ukuran_kaos_peserta }}</td>
                <td style="text-align: center">{{ $data->ikut ?? "Belum Konfirmasi" }}</td>
            </tr>
            <tr>
                <td>{{ $data->istri }}</td>
                <td>Istri</td>
                <td style="text-align: center">{{ $data->ukuran_kaos_istri }}</td>
                <td style="text-align: center">{{ $data->istri_hadir ?? "Belum Konfirmasi" }}</td>
            </tr>
            @if ($data->anak)
            @php
                $anak_hadir =   json_decode($data->anak_ikut) ;
            @endphp
            @foreach (json_decode($data->anak) as $i => $item)
            @if ($item)
            @if ($data->anak_ikut)
            @php
                if ($anak_hadir[$i] == 'ikut') {
                    $hadir  +=  1 ;
                }
            @endphp
            @endif
            <tr>
                <td>{{ $item }}</td>
                <td>Anak</td>
                <td></td>
                <td style="text-align: center">
                    @if ($data->anak_ikut)
                    {{ $anak_hadir[$i] == 'null' ? "Belum Konfirmasi" : ($anak_hadir[$i] ?? "Belum Konfirmasi") }}
                    @else
                    Belum Konfirmasi
                    @endif
                </td>
            </tr>
            @endif
            @endforeach
            @endif

            @if ($data->nama_lain)
            @php
                $status     =   json_decode($data->status_lain) ;
                $lain_hadir =   json_decode($data->lain_ikut) ;
            @endphp
            @foreach (json_decode($data->nama_lain) as $i => $item)
                @if ($data->lain_ikut)
                    @if ($item)
                    @php
                        if ($lain_hadir[$i] == 'ikut') {
                            $hadir  +=  1 ;
                        }
                    @endphp
                @endif
                <tr>
                    <td>{{ $item }}</td>
                    <td>{{ $status[$i] }}</td>
                    <td></td>
                    <td style="text-align: center">
                        @if ($data->lain_ikut)
                            {{ $lain_hadir[$i] ?? "Belum Konfirmasi" }}
                        @else
                            Belum Konfirmasi
                        @endif
                    </td>
                </tr>
                @endif
            @endforeach
            @endif
        </tbody>
    </table>

    <table>
        <tr>
            <td style="width: 250px"><b>Penjemputan</b><br> {{ $data->titik_penjemputan ?? 'Belum Dikonfirmasi' }}</td>
            <td><b>Jumlah Peserta Datang</b><br><span style="padding:5px 10px; color: #252525">{{ $hadir }} Orang</span></td>
        </tr>
    </table>


    <table width="100%" style="margin-top: 40px; border-collapse: collapse">
        <tbody>
            <tr>
                <td style="width: 120px" rowspan="2"><img src="sahidraya.png" style="width: 120px"></td>
                <td><b>Type Kamar</b><br> {{ $data->type_kamar ?? "Belum Dikonfirmasi" }}</td>
                <td>
                    @php
                        $tambahan_kamar =   0 ;
                    @endphp
                    <b>Tambahan Kamar :</b> <br>
                    @if ($data->type_kamar_tambahan)
                        @php
                            $jumlah =   json_decode($data->jumlah_tambahan) ;
                        @endphp
                        <ul style="margin: 0">
                            @foreach (json_decode($data->type_kamar_tambahan) as $i => $item)
                                @if ($item)
                                @php
                                    $tambahan_kamar +=  $jumlah[$i] ;
                                @endphp
                                <li>{{ $item }} : {{ $jumlah[$i] }} Kamar</li>
                                @endif
                            @endforeach        
                        </ul>
                    @else
                        Tidak Ada
                    @endif
                </td>
                @php
                    if ($data->ikut == 'ikut') {
                        $hadir  +=  1 ;
                    }
                    if ($data->istri_hadir == 'ikut') {
                        $hadir  +=  1 ;
                    }
                @endphp
                
            </tr>
            <tr>
                <td colspan="2"><b>Informasi :</b><br>
                    @if ($tambahan_kamar > 0)
                    Tambahan kamar dikenakan biaya Rp {{ number_format(550000*$tambahan_kamar) }}
                    @else
                    -
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
    
</body>
</html>