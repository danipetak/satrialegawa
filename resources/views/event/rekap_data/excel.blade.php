@php
    header('Content-Transfer-Encoding: none');
    header("Content-type: application/vnd-ms-excel");
    header("Content-type: application/x-msexcel");
    header("Content-Disposition: attachment; filename=Rekap Data Kehadiran.xls");
@endphp

<table border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Email</th>
            <th>NRP</th>
            <th>Telepon</th>
            <th>Tanggal Lahir</th>
            <th>Instagram</th>
            <th>Pangkat</th>
            <th>Korp</th>
            <th>Jabatan</th>
            <th>Kesatauan</th>
            <th>Alamat Surat Menyurat</th>
            <th>Asal Daerah</th>
            <th>Jabatan Taruna</th>
            <th>Kamar Tingkat 3</th>
            <th>Kopel</th>
            <th>Check Poin Pesiar</th>
            <th>Istri</th>
            <th>Anak</th>
            <th>Umur Anak</th>
            <th>Keluarga Lain</th>
            <th>Kaos Peserta</th>
            <th>Kaos Istri</th>
            <th>Hadir</th>
            <th>Mungkin</th>
            <th>Tidak</th>
            <th>Total</th>
            <th>Titik Penjemputan</th>
            <th>Tipe Kamar</th>
            <th>Tipe Kamar Tambahan</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $i => $row)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $row->name }}</td>
            <td>{{ $row->email }}</td>
            <td>{{ $row->nrp }}</td>
            <td>{{ $row->telepon }}</td>
            <td>{{ $row->tanggal_lahir }}</td>
            <td>{{ $row->instagram }}</td>
            <td>{{ $row->pangkat }}</td>
            <td>{{ $row->korp }}</td>
            <td>{{ $row->jabatan }}</td>
            <td>{{ $row->kesatuan }}</td>
            <td>{{ $row->alamat_rumah }}</td>
            <td>{{ $row->asal_daerah }}</td>
            <td>{{ $row->jabatan_taruna }}</td>
            <td>{{ $row->kamar_tingkat3 }}</td>
            <td>{{ $row->kopel }}</td>
            <td>{{ $row->check_poin_pesiar }}</td>
            <td>{{ $row->istri }}</td>
            <td>
                @if ($row->anak)
                @php
                    $umur       =   json_decode($row->umur) ;
                    $jumlah_ank =   0 ;
                @endphp
                    @foreach (json_decode($row->anak) as $i => $item)
                    @if ($item)
                    @php
                        $jumlah_ank +=  1 ;
                    @endphp
                    <div>{{ $item }}</div>
                    @endif
                    @endforeach
                @endif
            </td>
            <td>
                @for ($x = 0; $x < $jumlah_ank; $x++)
                <div>{{ $umur[$x] }}</div>
                @endfor
            </td>
            <td>
                @if ($row->nama_lain)
                @php
                    $status =   json_decode($row->status_lain) ;
                @endphp
                @foreach (json_decode($row->nama_lain) as $i => $item)
                    @if ($item)
                    <div>{{ $item }} ({{ $status[$i] }})</div>
                    @endif
                @endforeach
                @endif
            </td>
            @php
            $hadir      =   0 ;
            $mungkin    =   0 ;
            $tidak      =   0 ;

            $hadir      +=  $row->ikut  ==  'ikut' ? 1 : 0 ;
            $mungkin    +=  $row->ikut  ==  'mungkin' ? 1 : 0 ;
            $tidak      +=  $row->ikut  ==  'tidak' ? 1 : 0 ;

            if ($row->anak_ikut) {
                $anak_hadir =   json_decode($row->anak_ikut) ;
                for ($i=0; $i < COUNT($anak_hadir); $i++) {
                    if ($anak_hadir[$i] == 'ikut') {
                        $hadir      +=   1 ;
                    }
                    if ($anak_hadir[$i] == 'mungkin') {
                        $mungkin    +=   1 ;
                    }
                    if ($anak_hadir[$i] == 'null') {
                        $tidak      +=   1 ;
                    }
                }
            }

            if ($row->lain_ikut) {
                $lain_hadir =   json_decode($row->lain_ikut) ;
                for ($i=0; $i < COUNT($lain_hadir); $i++) {
                    if ($lain_hadir[$i] == 'ikut') {
                        $hadir      +=   1 ;
                    }
                    if ($lain_hadir[$i] == 'mungkin') {
                        $mungkin    +=   1 ;
                    }
                    if ($lain_hadir[$i] == 'null') {
                        $tidak      +=   1 ;
                    }
                }
            }

            if ($row->istri_hadir) {
                if ($row->istri_hadir == 'ikut') {
                    $hadir      +=   1 ;
                }
                if ($row->istri_hadir == 'mungkin') {
                    $mungkin    +=   1 ;
                }
                if ($row->istri_hadir == 'null') {
                    $tidak      +=   1 ;
                }
                if ($row->istri_hadir == 'tidak') {
                    $tidak      +=   1 ;
                }
            }
            @endphp
            <td>{{ $row->ukuran_kaos_peserta }}</td>
            <td>{{ $row->ukuran_kaos_istri }}</td>
            <td>{{ $hadir }}</td>
            <td>{{ $mungkin }}</td>
            <td>{{ $tidak }}</td>
            <td>{{ ($hadir + $mungkin + $tidak) }}</td>
            <td>{{ $row->titik_penjemputan }}</td>
            <td>{{ $row->type_kamar }}</td>
            <td>
                @php
                    $jumlah =   json_decode($row->jumlah_tambahan) ;
                @endphp
                @if ($row->type_kamar_tambahan)
                @foreach (json_decode($row->type_kamar_tambahan) as $i => $item)
                    @if ($item)
                    <div>{{ $item }} ({{ $jumlah[$i] }})</div>
                    @endif
                @endforeach
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
