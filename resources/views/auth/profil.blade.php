@extends('layouts.app')

@section('title', "Identitas Pribadi")

@section('header')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset("vendor/plugins/icheck-bootstrap/icheck-bootstrap.min.css") }}">
@endsection

@section('footer')
<script src="{{ asset('js/select2.full.js') }}"></script>

<script>
$("input[name=ikut]").on('click', function() {
    if ($(this).val() == 'ikut') {
        $("#informasi_tidak").attr("style", "display: none") ;
        $("#informasi_mungkin").attr("style", "display: none") ;
        $("#informasi_ikut").attr("style", "display: block") ;
    }
    if ($(this).val() == 'mungkin') {
        $("#informasi_tidak").attr("style", "display: none") ;
        $("#informasi_mungkin").attr("style", "display: block") ;
        $("#informasi_ikut").attr("style", "display: none") ;
    }
    if ($(this).val() == 'tidak') {
        $("#informasi_tidak").attr("style", "display: block") ;
        $("#informasi_mungkin").attr("style", "display: none") ;
        $("#informasi_ikut").attr("style", "display: none") ;
    }
})
</script>

<script>
var y = {{ Auth::user()->umur ? (COUNT(json_decode(Auth::user()->umur)) + 1) : 1 }};
function addRow(){
    var row = '';
    row +=  '<div class="mt-3 row row-'+(y)+'">' ;
    row +=  '   <div class="col-12 col-md mb-2 mb-md-0">' ;
    row +=  '       <input type="text" class="nama_anak form-control" placeholder="Tuliskan Nama Anak">' ;
    row +=  '   </div>' ;
    row +=  '   <div class="px-md-1 col col-md-2 col-lg-1">' ;
    row +=  '       <input type="text" class="umur_anak form-control" placeholder="Umur">' ;
    row +=  '   </div>' ;
    row +=  '   <div class="px-md-1 col col-md-2 col-lg-1">' ;
    row +=  '       <select class="anak_ikut form-control">' ;
    row +=  '           <option value="">Kehadiran</option>' ;
    row +=  '           <option value="ikut">Ikut</option>' ;
    row +=  '           <option value="mungkin">Mungkin</option>' ;
    row +=  '           <option value="tidak">Tidak</option>' ;
    row +=  '       </select>' ;
    row +=  '   </div>' ;
    row +=  '    <div class="col-auto">' ;
    row +=  '        <button type="button" onclick="deleteRow('+(y)+')" class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>' ;
    row +=  '    </div>' ;
    row +=  '</div>' ;

    $('.data-loop').append(row);
    $('.select2').select2();
    y++;
}

function deleteRow(rowid){
    $('.row-'+rowid).remove();
}
</script>

<script>
$("#update_identitas").on("click", function() {
    var instagram           =   $("#instagram").val() ;
    var tanggal_lahir       =   $("#tanggal_lahir").val() ;
    var pangkat             =   $("#pangkat").val() ;
    var korp                =   $("#korp").val() ;
    var jabatan             =   $("#jabatan").val() ;
    var kesatuan            =   $("#kesatuan").val() ;
    var alamat_rumah        =   $("#alamat_rumah").val() ;
    var asal_daerah         =   $("#asal_daerah").val() ;
    var jabatan_taruna      =   $("#jabatan_taruna  ").val() ;
    var kamar_tingkat3      =   $("#kamar_tingkat3").val() ;
    var kopel               =   $("#kopel").val() ;
    var check_poin_pesiar   =   $("#check_poin_pesiar").val() ;
    var ukuran_kaos_peserta =   $("#ukuran_kaos_peserta").val() ;
    var istri               =   $("#istri").val() ;
    var ukuran_kaos_istri   =   $("#ukuran_kaos_istri").val() ;
    var istri_hadir         =   $("#istri_hadir").val() ;
    var nama_anak           =   document.getElementsByClassName("nama_anak");
    var anak                =   [];

    var nama    =   0 ;
    for (var i = 0; i < nama_anak.length; ++i) {
        if (nama_anak[i].value) {
            nama    +=  nama_anak[i].value ? 1 : 0 ;
            anak.push(nama_anak[i].value);
        }
    }

    var umur_anak           =   document.getElementsByClassName("umur_anak");
    var umur                =   [];

    var umr =   0 ;
    for (var i = 0; i < umur_anak.length; ++i) {
        umr +=  umur_anak[i].value ? 1 : 0 ;
        umur.push(umur_anak[i].value);
    }

    var anak_ikut           =   document.getElementsByClassName("anak_ikut");
    var anakikut            =   [];

    for (var i = 0; i < nama_anak.length; ++i) {
        if (nama_anak[i].value) {
            if (anak_ikut[i].value) {
                anakikut.push(anak_ikut[i].value);
            } else {
                anakikut.push('null');
            }
        }
    }

    if (nama != umr) {
        showAlert("Data anak belum lengkap")
    } else {
        var ikut                =   $("input[name=ikut]:checked").val() ;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "{{ route('profil.store') }}",
            method: "POST",
            data: {
                instagram           :   instagram ,
                tanggal_lahir       :   tanggal_lahir ,
                pangkat             :   pangkat ,
                korp                :   korp ,
                jabatan             :   jabatan ,
                kesatuan            :   kesatuan ,
                alamat_rumah        :   alamat_rumah ,
                asal_daerah         :   asal_daerah ,
                jabatan_taruna      :   jabatan_taruna ,
                kamar_tingkat3      :   kamar_tingkat3 ,
                kopel               :   kopel ,
                check_poin_pesiar   :   check_poin_pesiar ,
                ukuran_kaos_peserta :   ukuran_kaos_peserta ,
                istri               :   istri ,
                ukuran_kaos_istri   :   ukuran_kaos_istri ,
                istri_hadir         :   istri_hadir ,
                anak                :   anak ,
                umur                :   umur ,
                anakikut            :   anakikut ,
                ikut                :   ikut
            },
            success: function(data) {
                if (data.status == 200) {
                    $("#info_submit").attr('style', 'display: block') ;
                    $("#form_submit").attr('style', 'display: none') ;
                    $("#submit_button").attr("style", "display: none") ;
                    document.getElementById("informasi_submit").innerHTML   =   data.msg ;
                } else if (data.status == 400) {
                    showAlert(data.msg)
                } else {
                    $("#loading_data").attr('style', 'display: block') ;
                    $("#submit_button").attr('style', 'display: none') ;
                    window.location.href =  "{{ route('reuniaau.index') }}"
                }
            }
        });
    }

})
</script>

<script>
$("#form_ubah_anak").on('click', function() {
    $("#info_tambahan").attr('style', 'display: block') ;
    $("#anak_tambahan").attr('style', 'display: block') ;
})
</script>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <b>Data Identitas Pribadi</b>
    </div>
    <div class="card-body">
        <b>Tanda <span class="text-danger">*</span> Wajib Di Isi</b>
        <hr>
        <div class="form-group">
            <label for="nama_lengkap">Nama Lengkap</label>
            <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" disabled value="{{ Auth::user()->name }}">
        </div>
        <div class="form-group">
            <label for="nrp">NRP</label>
            <input type="text" name="nrp" id="nrp" class="form-control" disabled value="{{ Auth::user()->nrp }}">
        </div>
        <div class="form-group">
            <label for="instagram">Akun Instagram</label>
            <input type="text" name="instagram" id="instagram" class="form-control" value="{{ Auth::user()->instagram }}">
        </div>
        <div class="form-group">
            <label for="tanggal_lahir">Tanggal Lahir <span class="text-danger">*</span></label>
            <input type="date" name="tanggal_lahir" id="tanggal_lahir" value="{{ Auth::user()->tanggal_lahir ?? '' }}" class="form-control" required autocomplete="off">
        </div>
        <div class="form-group">
            <label for="pangkat">Pangkat / Korp <span class="text-danger">*</span></label>
            <div class="row">
                <div class="col">
                    <input type="text" name="pangkat" id="pangkat" value="{{ Auth::user()->pangkat ?? '' }}" class="form-control" placeholder="Tuliskan Pangkat" required autocomplete="off">
                </div>
                <div class="col">
                    <select name="korp" id="korp" class="form-control">
                        <option value="" disabled selected hidden>Pilih Korp</option>
                        <option value="PNB" {{ Auth::user()->korp == "PNB" ? "selected" : "" }}>PNB</option>
                        <option value="TEK" {{ Auth::user()->korp == "TEK" ? "selected" : "" }}>TEK</option>
                        <option value="LEK" {{ Auth::user()->korp == "LEK" ? "selected" : "" }}>LEK</option>
                        <option value="ADM" {{ Auth::user()->korp == "ADM" ? "selected" : "" }}>ADM</option>
                        <option value="KAL" {{ Auth::user()->korp == "KAL" ? "selected" : "" }}>KAL</option>
                        <option value="POM" {{ Auth::user()->korp == "POM" ? "selected" : "" }}>POM</option>
                        <option value="SUS" {{ Auth::user()->korp == "SUS" ? "selected" : "" }}>SUS</option>
                        <option value="PGT" {{ Auth::user()->korp == "PGT" ? "selected" : "" }}>PGT</option>
                        <option value="PENSIUN DINI" {{ Auth::user()->korp == "PENSIUN DINI" ? "selected" : "" }}>PENSIUN DINI</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="jabatan">Jabatan <span class="text-danger">*</span></label>
            <input type="text" name="jabatan" id="jabatan" value="{{ Auth::user()->jabatan ?? '' }}" class="form-control" placeholder="Tuliskan Jabatan" required autocomplete="off">
        </div>
        <div class="form-group">
            <label for="kesatuan">Kesatuan <span class="text-danger">*</span></label>
            <input type="text" name="kesatuan" id="kesatuan" value="{{ Auth::user()->kesatuan ?? '' }}" class="form-control" placeholder="Tuliskan Kesatuan" required autocomplete="off">
        </div>
        <div class="form-group">
            <label for="alamat_rumah">Alamat Surat Menyurat <span class="text-danger">*</span></label>
            <input type="text" name="alamat_rumah" id="alamat_rumah" value="{{ Auth::user()->alamat_rumah ?? '' }}" class="form-control" placeholder="Tuliskan Alamat Surat Menyurat" required autocomplete="off">
        </div>
        <div class="form-group">
            <label for="asal_daerah">Asal Daerah <span class="text-danger">*</span></label>
            <select name="asal_daerah" id="asal_daerah" class="form-control select2" data-placeholder="Pilih Asal Daerah" data-width="100%" required>
                <option value=""></option>
                <option value="Aceh" {{ Auth::user()->asal_daerah == "Aceh" ? "selected" : "" }}>Aceh</option>
                <option value="Sumatra Utara" {{ Auth::user()->asal_daerah == "Sumatra Utara" ? "selected" : "" }}>Sumatra Utara</option>
                <option value="Sumatra Barat" {{ Auth::user()->asal_daerah == "Sumatra Barat" ? "selected" : "" }}>Sumatra Barat</option>
                <option value="Riau" {{ Auth::user()->asal_daerah == "Riau" ? "selected" : "" }}>Riau</option>
                <option value="Jambi" {{ Auth::user()->asal_daerah == "Jambi" ? "selected" : "" }}>Jambi</option>
                <option value="Kepulauan Riau" {{ Auth::user()->asal_daerah == "Kepulauan Riau" ? "selected" : "" }}>Kepulauan Riau</option>
                <option value="Bengkulu" {{ Auth::user()->asal_daerah == "Bengkulu" ? "selected" : "" }}>Bengkulu</option>
                <option value="Sumatra Selatan" {{ Auth::user()->asal_daerah == "Sumatra Selatan" ? "selected" : "" }}>Sumatra Selatan</option>
                <option value="Kepulauan Bangka Belitung" {{ Auth::user()->asal_daerah == "Kepulauan Bangka Belitung" ? "selected" : "" }}>Kepulauan Bangka Belitung</option>
                <option value="Lampung" {{ Auth::user()->asal_daerah == "Lampung" ? "selected" : "" }}>Lampung</option>
                <option value="Banten" {{ Auth::user()->asal_daerah == "Banten" ? "selected" : "" }}>Banten</option>
                <option value="Daerah Khusus Ibukota Jakarta" {{ Auth::user()->asal_daerah == "Daerah Khusus Ibukota Jakarta" ? "selected" : "" }}>Daerah Khusus Ibukota Jakarta</option>
                <option value="Jawa Barat" {{ Auth::user()->asal_daerah == "Jawa Barat" ? "selected" : "" }}>Jawa Barat</option>
                <option value="Jawa Tengah" {{ Auth::user()->asal_daerah == "Jawa Tengah" ? "selected" : "" }}>Jawa Tengah</option>
                <option value="Daerah Istimewa Yogyakarta" {{ Auth::user()->asal_daerah == "Daerah Istimewa Yogyakarta" ? "selected" : "" }}>Daerah Istimewa Yogyakarta</option>
                <option value="Jawa Timur" {{ Auth::user()->asal_daerah == "Jawa Timur" ? "selected" : "" }}>Jawa Timur</option>
                <option value="Bali" {{ Auth::user()->asal_daerah == "Bali" ? "selected" : "" }}>Bali</option>
                <option value="Nusa Tenggara Barat" {{ Auth::user()->asal_daerah == "Nusa Tenggara Barat" ? "selected" : "" }}>Nusa Tenggara Barat</option>
                <option value="Nusa Tenggara Timur" {{ Auth::user()->asal_daerah == "Nusa Tenggara Timur" ? "selected" : "" }}>Nusa Tenggara Timur</option>
                <option value="Kalimantan Barat" {{ Auth::user()->asal_daerah == "Kalimantan Barat" ? "selected" : "" }}>Kalimantan Barat</option>
                <option value="Kalimantan Tengah" {{ Auth::user()->asal_daerah == "Kalimantan Tengah" ? "selected" : "" }}>Kalimantan Tengah</option>
                <option value="Kalimantan Selatan" {{ Auth::user()->asal_daerah == "Kalimantan Selatan" ? "selected" : "" }}>Kalimantan Selatan</option>
                <option value="Kalimantan Timur" {{ Auth::user()->asal_daerah == "Kalimantan Timur" ? "selected" : "" }}>Kalimantan Timur</option>
                <option value="Kalimantan Utara" {{ Auth::user()->asal_daerah == "Kalimantan Utara" ? "selected" : "" }}>Kalimantan Utara</option>
                <option value="Sulawesi Barat" {{ Auth::user()->asal_daerah == "Sulawesi Barat" ? "selected" : "" }}>Sulawesi Barat</option>
                <option value="Sulawesi Selatan" {{ Auth::user()->asal_daerah == "Sulawesi Selatan" ? "selected" : "" }}>Sulawesi Selatan</option>
                <option value="Sulawesi Tenggara" {{ Auth::user()->asal_daerah == "Sulawesi Tenggara" ? "selected" : "" }}>Sulawesi Tenggara</option>
                <option value="Sulawesi Tengah" {{ Auth::user()->asal_daerah == "Sulawesi Tengah" ? "selected" : "" }}>Sulawesi Tengah</option>
                <option value="Gorontalo" {{ Auth::user()->asal_daerah == "Gorontalo" ? "selected" : "" }}>Gorontalo</option>
                <option value="Sulawesi Utara" {{ Auth::user()->asal_daerah == "Sulawesi Utara" ? "selected" : "" }}>Sulawesi Utara</option>
                <option value="Maluku Utara" {{ Auth::user()->asal_daerah == "Maluku Utara" ? "selected" : "" }}>Maluku Utara</option>
                <option value="Maluku" {{ Auth::user()->asal_daerah == "Maluku" ? "selected" : "" }}>Maluku</option>
                <option value="Papua Barat" {{ Auth::user()->asal_daerah == "Papua Barat" ? "selected" : "" }}>Papua Barat</option>
                <option value="Papua" {{ Auth::user()->asal_daerah == "Papua" ? "selected" : "" }}>Papua</option>
                <option value="Papua Tengah" {{ Auth::user()->asal_daerah == "Papua Tengah" ? "selected" : "" }}>Papua Tengah</option>
                <option value="Papua Pegunungan" {{ Auth::user()->asal_daerah == "Papua Pegunungan" ? "selected" : "" }}>Papua Pegunungan</option>
                <option value="Papua Selatan" {{ Auth::user()->asal_daerah == "Papua Selatan" ? "selected" : "" }}>Papua Selatan</option>
            </select>
        </div>
        <div class="form-group">
            <label for="jabatan_taruna">Jabatan di Taruna <span class="text-danger">*</span></label>
            <input type="text" name="jabatan_taruna" id="jabatan_taruna" value="{{ Auth::user()->jabatan_taruna ?? "" }}" class="form-control" placeholder="Tuliskan Jabatan di Taruna" required autocomplete="off">
        </div>
        <div class="form-group">
            <label for="">No. Kamar Tingkat 3 <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="kamar_tingkat3" value="{{ Auth::user()->kamar_tingkat3 ?? "" }}" autocomplete="off" required placeholder="Tuliskan No. Kamar Tingkat 3">
        </div>

        <div class="form-group">
            <label for="">Kopel <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="kopel" value="{{ Auth::user()->kopel ?? "" }}" autocomplete="off" required placeholder="Tuliskan Kopel">
        </div>

        <div class="form-group">
            <label for="">Check Poin Pesiar <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="check_poin_pesiar" value="{{ Auth::user()->check_poin_pesiar ?? "" }}" autocomplete="off" required placeholder="Tuliskan Check Poin Pesiar">
        </div>

        <b>Tanda <span class="text-danger">*</span> Wajib Di Isi</b>
    </div>
</div>

<div class="card">
    <div class="card-header"><b>Data Keluarga</b></div>
    <div class="card-body">
        <div class="form-group">
            <div class="row">
                <div class="col pr-1">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" autocomplete="off" value="{{ Auth::user()->name }}" disabled>
                </div>
                <div class="col-4 col-md-2 pl-1">
                    <label for="ukuran_kaos_peserta">Ukuran Kaos <span class="text-danger">*</span></label>
                    <select name="ukuran_kaos_peserta" id="ukuran_kaos_peserta" class="form-control">
                        <option value="" disabled selected hidden>Ukuran Kaos</option>
                        <option value="S" {{ Auth::user()->ukuran_kaos_peserta == "S" ? "selected" : "" }}>S</option>
                        <option value="M" {{ Auth::user()->ukuran_kaos_peserta == "M" ? "selected" : "" }}>M</option>
                        <option value="L" {{ Auth::user()->ukuran_kaos_peserta == "L" ? "selected" : "" }}>L</option>
                        <option value="XL" {{ Auth::user()->ukuran_kaos_peserta == "XL" ? "selected" : "" }}>XL</option>
                        <option value="XXL" {{ Auth::user()->ukuran_kaos_peserta == "XXL" ? "selected" : "" }}>XXL</option>
                        <option value="XXXL" {{ Auth::user()->ukuran_kaos_peserta == "XXXL" ? "selected" : "" }}>XXXL</option>
                        <option value="XXXXL" {{ Auth::user()->ukuran_kaos_peserta == "XXXXL" ? "selected" : "" }}>XXXXL</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-12 col-md pr-0 pr-md-1 mb-3 mb-md-0">
                    <label for="istri">Istri</label>
                    <input type="text" class="form-control" id="istri" value="{{ Auth::user()->istri ?? "" }}" autocomplete="off" placeholder="Tuliskan Istri">
                </div>
                <div class="col-6 col-md-2 pl-1">
                    <label for="ukuran_kaos_istri">Ukuran Kaos</label>
                    <select name="ukuran_kaos_istri" id="ukuran_kaos_istri" class="form-control">
                        <option value="" disabled selected hidden>Ukuran Kaos</option>
                        <option value="S" {{ Auth::user()->ukuran_kaos_istri == "S" ? "selected" : "" }}>S</option>
                        <option value="M" {{ Auth::user()->ukuran_kaos_istri == "M" ? "selected" : "" }}>M</option>
                        <option value="L" {{ Auth::user()->ukuran_kaos_istri == "L" ? "selected" : "" }}>L</option>
                        <option value="XL" {{ Auth::user()->ukuran_kaos_istri == "XL" ? "selected" : "" }}>XL</option>
                        <option value="XXL" {{ Auth::user()->ukuran_kaos_istri == "XXL" ? "selected" : "" }}>XXL</option>
                        <option value="XXXL" {{ Auth::user()->ukuran_kaos_istri == "XXXL" ? "selected" : "" }}>XXXL</option>
                        <option value="XXXXL" {{ Auth::user()->ukuran_kaos_istri == "XXXXL" ? "selected" : "" }}>XXXXL</option>
                    </select>
                </div>
                <div class="col-6 col-md-2 pl-1">
                    <label for="istri_hadir">Kehadiran</label>
                    <select name="istri_hadir" id="istri_hadir" class="form-control">
                        <option value="" selected disabled hidden>Kehadiran</option>
                        <option value="ikut" {{ Auth::user()->istri_hadir == "ikut" ? "selected" : "" }}>Ikut</option>
                        <option value="mungkin" {{ Auth::user()->istri_hadir == "mungkin" ? "selected" : "" }}>Mungkin</option>
                        <option value="tidak" {{ Auth::user()->istri_hadir == "tidak" ? "selected" : "" }}>Tidak</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="anak">Anak</label>
            @if (Auth::user()->anak)
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Umur</th>
                        <th>Kehadiran</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $umur       =   json_decode(Auth::user()->umur) ;
                        $anak_ikut  =   json_decode(Auth::user()->anak_ikut) ;
                    @endphp
                    @foreach (json_decode(Auth::user()->anak) as $i => $row)
                    <tr>
                        <td>{{ $row }}</td>
                        <td>{{ $umur[$i] ?? '' }}</td>
                        <td>{{ $anak_ikut ? ($anak_ikut[$i] == 'null' ? '' : $anak_ikut[$i] ?? '') : '' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <button class="btn btn-sm btn-primary mb-4" id="form_ubah_anak" type="button">Ubah Data Anak</button>
            @endif
            <div style="display: none" id="info_tambahan" class="font-weight-bold mb-2">Isi Kembali Data Anak</div>
            <div class="data-loop" id="anak_tambahan" @if (Auth::user()->anak) style="display: none" @endif>
                @if (Auth::user()->anak)
                    @php
                        $umur       =   json_decode(Auth::user()->umur) ;
                        $anak_ikut  =   json_decode(Auth::user()->anak_ikut) ;
                    @endphp
                    @foreach (json_decode(Auth::user()->anak) as $i => $row)
                    <div class="row row-{{ ($i + 1) }} mb-3">
                        <div class="col-12 col-md mb-2 mb-md-0">
                            <input type="text" class="nama_anak form-control" value="{{ $row }}" placeholder="Tuliskan Nama Anak">
                        </div>
                        <div class="px-md-1 col col-md-2 col-lg-1">
                            <input type="text" class="umur_anak form-control" value="{{ $umur[$i] }}" placeholder="Umur">
                        </div>
                        <div class="px-md-1 col col-md-2 col-lg-1">
                            <select class="anak_ikut form-control">
                                <option value="" disabled hidden selected>Kehadiran</option>
                                <option value="ikut" {{ $anak_ikut ? ($anak_ikut[$i] == "ikut" ? "selected" : "") : "" }}>Ikut</option>
                                <option value="mungkin" {{ $anak_ikut ? ($anak_ikut[$i] == "mungkin" ? "selected" : "") : "" }}>Mungkin</option>
                                <option value="tidak" {{ $anak_ikut ? ($anak_ikut[$i] == "tidak" ? "selected" : "") : "" }}>Tidak</option>
                            </select>
                        </div>
                        <div class="col-auto">
                            <button type="button" onclick="deleteRow({{ ($i + 1) }})" class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    @endforeach
                    <div class="row">
                        <div class="col-12 col-md mb-2 mb-md-0">
                            <input type="text" class="nama_anak form-control" placeholder="Tuliskan Nama Anak">
                        </div>
                        <div class="px-md-1 col col-md-2 col-lg-1">
                            <input type="text" class="umur_anak form-control" placeholder="Umur">
                        </div>
                        <div class="px-md-1 col col-md-2 col-lg-1">
                            <select class="anak_ikut form-control">
                                <option value="">Kehadiran</option>
                                <option value="ikut">Ikut</option>
                                <option value="mungkin">Mungkin</option>
                                <option value="tidak">Tidak</option>
                            </select>
                        </div>
                        <div class="col-auto">
                            <button type="button" onclick="addRow()" class="btn btn-outline-success"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                @else
                <div class="row">
                    <div class="col-12 col-md mb-2 mb-md-0">
                        <input type="text" class="nama_anak form-control" placeholder="Tuliskan Nama Anak">
                    </div>
                    <div class="px-md-1 col col-md-2 col-lg-1">
                        <input type="text" class="umur_anak form-control" placeholder="Umur">
                    </div>
                    <div class="px-md-1 col col-md-2 col-lg-1">
                        <select class="anak_ikut form-control">
                            <option value="" disabled hidden selected>Kehadiran</option>
                            <option value="ikut">Ikut</option>
                            <option value="mungkin">Mungkin</option>
                            <option value="tidak">Tidak</option>
                        </select>
                    </div>
                    <div class="col-auto">
                        <button type="button" onclick="addRow()" class="btn btn-outline-success"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="card-footer text-right">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#lanjutIdentitas">
            Lanjut
        </button>
    </div>
</div>

<div class="modal fade" id="lanjutIdentitas" tabindex="-1" aria-labelledby="lanjutIdentitasLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div id="form_submit">
                    <h5 class="text-center mt-3 mb-4">Ikut Reuni?</h5>
                    <div class="row">
                        <div class="col text-center">
                            <input type="radio" name="ikut" id="ikut" {{ Auth::user()->ikut == 'ikut' ? "checked" : ''}} value="ikut">
                            <label for="ikut">Ikut</label>
                        </div>
                        <div class="col text-center">
                            <input type="radio" name="ikut" id="mungkin" {{ Auth::user()->ikut == 'mungkin' ? "checked" : ''}} value="mungkin">
                            <label for="mungkin">Mungkin</label>
                        </div>
                        <div class="col text-center">
                            <input type="radio" name="ikut" id="tidak" {{ Auth::user()->ikut == 'tidak' ? "checked" : ''}} value="tidak">
                            <label for="tidak">Tidak</label>
                        </div>
                    </div>
                    <div class="text-center" id="informasi_ikut" style="{{ Auth::user()->ikut == "ikut" ? "" : "display: none" }}">
                        Sak taeek!!!<br>
                        Acarane mesti gayeng
                    </div>
                    <div class="text-center" id="informasi_mungkin" style="{{ Auth::user()->ikut == "mungkin" ? "" : "display: none" }}">
                        Sing penting yakin!!!<br>
                        Gak ada lo gak rame...
                    </div>
                    <div class="text-center" id="informasi_tidak" style="{{ Auth::user()->ikut == "tidak" ? "" : "display: none" }}">
                        Asuuu og...!!!!!<br>
                        Mbok melu.... Mesakke panitia sing nyiapke
                    </div>
                </div>
                <h5 class="text-center mt-3" style="display: none" id="loading_data"><i class="fas fa-sync-alt fa-spin"></i> Loading......</h5>
                <div id="info_submit" class="text-center" style="display: none">
                    <div class="alert alert-info" id="informasi_submit"></div>
                    <a class="btn btn-outline-danger mb-3 mb-sm-0" href="{{ route('dashboard') }}">Kembali Ke Halaman Depan</a>&nbsp;
                    <a class="btn btn-primary" href="{{ route('profil.index') }}">Tetap di Halaman Ini</a>
                </div>
            </div>
            <div class="modal-footer">
                <div id="submit_button">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="update_identitas" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
