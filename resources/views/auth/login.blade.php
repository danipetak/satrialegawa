@extends('layouts.auth')

@section('title', 'Login')

@section('content')
<div class="login-box">
    <div class="login-logo" style="font-family: 'DM Serif Display', serif;">
        {{-- <img src="{{ asset('assets/img/sedulur_saklawase.png') }}" style="width: 300px"> --}}
        Satria Legawa
        <div class="small">2002/2022</div>
    </div>

    <div class="card">
        <div class="card-body login-card-body">
        {{-- <p class="login-box-msg">&nbsp;</p> --}}

        <form action="{{ route('authenticate') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="email">E-Mail</label>
                <input type="text" name="email" id="email" class="form-control" placeholder="Tuliskan E-Mail" value="{{ old('email') }}">
                @error('email') <div class="text-danger small">{{ $message }}</div> @enderror
            </div>

            <div class="form-group">
                <label for="password">NRP</label>
                <input type="password" name="password" id="password" class="form-control" autocomplete="off" placeholder="Tuliskan NRP">
            </div>

            <div class="row">
                <div class="col-8">
                    <a href="{{ route('home') }}#registrasi" class="btn btn-outline-dark"><i class="fas fa-arrow-left"></i> Daftar</a>
                </div>

                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-sign-in-alt"></i> Masuk</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
