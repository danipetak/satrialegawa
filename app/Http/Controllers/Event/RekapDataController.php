<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use PDF ;

class RekapDataController extends Controller
{
    public function index(Request $request)
    {
        if ($request->key == 'unduh') {
            $data   =   User::where(function($query) {
                            $query->orWhere('status', NULL) ;
                            $query->orWhere('status', 1) ;
                        })->get() ;
                        
            return view('event.rekap_data.excel', compact('data')) ;
        } else

        if ($request->key == 'view') {
            $data   =   User::where(function($query) use ($request) {
                            if ($request->search) {
                                $query->orWhere('name', 'like', '%' . $request->search . '%') ;
                                $query->orWhere('email', 'like', '%' . $request->search . '%') ;
                                $query->orWhere('telepon', 'like', '%' . $request->search . '%') ;
                                $query->orWhere('pangkat', 'like', '%' . $request->search . '%') ;
                                $query->orWhere('korp', 'like', '%' . $request->search . '%') ;
                                $query->orWhere('ikut', 'like', '%' . $request->search . '%') ;
                                $query->orWhere('nrp', 'like', '%' . $request->search . '%') ;
                                $query->orWhere('titik_penjemputan', 'like', '%' . $request->search . '%') ;
                                $query->orWhere('type_kamar', 'like', '%' . $request->search . '%') ;
                                $query->orWhere('ukuran_kaos_peserta', 'like', '%' . $request->search . '%') ;
                            }
                        })
                        ->where(function($query) use ($request) {
                            if ($request->kehadiran) {
                                if ($request->kehadiran != 'all') {
                                    if ($request->kehadiran == 'belumisi') {
                                        $query->where('ikut', NULL) ;
                                    } else {
                                        $query->where('ikut', $request->kehadiran) ;
                                    }
                                }
                            }
                        })
                        ->where(function($query) {
                            $query->orWhere('status', NULL) ;
                            $query->orWhere('status', 1) ;
                        })
                        ->orderBy('nrp')
                        ->get();

            return view('event.rekap_data.view', compact('data'));
        } else {
            $hadir      =   User::where(function($query) {
                                $query->orWhere('status', NULL) ;
                                $query->orWhere('status', 1) ;
                            })
                            ->where('ikut', 'ikut')->count() ;

            $mungkin    =   User::where(function($query) {
                                $query->orWhere('status', NULL) ;
                                $query->orWhere('status', 1) ;
                            })
                            ->where('ikut', 'mungkin')->count() ;

            $tidak      =   User::where(function($query) {
                                $query->orWhere('status', NULL) ;
                                $query->orWhere('status', 1) ;
                            })
                            ->where('ikut', 'tidak')->count() ;

            $king       =   User::where(function($query) {
                                $query->orWhere('status', NULL) ;
                                $query->orWhere('status', 1) ;
                            })
                            ->where('type_kamar', 'King')->count() ;

            $twin       =   User::where(function($query) {
                                $query->orWhere('status', NULL) ;
                                $query->orWhere('status', 1) ;
                            })
                            ->where('type_kamar', 'Twin')->count() ;


            foreach (User::where(function($query) {
                            $query->orWhere('status', NULL) ;
                            $query->orWhere('status', 1) ;
                        })
                        ->where('type_kamar_tambahan', '!=', NULL)->get() as $row) {
                $exp    =   json_decode($row->type_kamar_tambahan) ;
                $tambah =   json_decode($row->jumlah_tambahan) ;

                foreach ($exp as $i => $list) {
                    $king   +=  $list == "King" ? $tambah[$i] : 0 ;
                    $twin   +=  $list == "Twin" ? $tambah[$i] : 0 ;
                }

            }



            $phadir     =   0;
            foreach (User::get() as $row) {
                if ($row->anak_ikut) {
                    $anak_hadir =   json_decode($row->anak_ikut);
                    for ($i = 0; $i < COUNT($anak_hadir); $i++) {
                        if ($anak_hadir[$i] == 'ikut') {
                            $phadir     +=   1;
                        }
                    }
                }

                if ($row->istri_hadir) {
                    if ($row->istri_hadir == 'ikut') {
                        $phadir      +=   1;
                    }
                }
            }

            $peserta    =   $phadir + User::where('ikut', 'ikut')->count() ;

            return view('event.rekap_data.index', compact('hadir', 'peserta', 'mungkin', 'tidak', 'king', 'twin')) ;
        }
    }

    public function kehadiran(Request $request)
    {
        $data       =   $request->id ? User::find($request->id) : Auth::user() ; 
        $pdf        =   App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadHTML(view('event.rekap_data.pdf', compact('data')));
        return $pdf->stream();
    }
}
