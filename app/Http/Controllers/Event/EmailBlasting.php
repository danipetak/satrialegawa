<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail ;

class EmailBlasting extends Controller
{
    public function index(Request $request)
    {
        return view('event.blasting.index');
    }

    public function store(Request $request)
    {
        foreach (User::get() as $row) {
            $to_name    =   $row->name;
            $to_email   =   $row->email;
            $data       =   ["nama" => $to_name, "pesan" => $request->message] ;
            
            Mail::send('auth.mailblast', $data, function($message) use ($to_name, $to_email, $request) {
                $message->to($to_email, $to_name)->subject($request->subject);
                $message->from('satrialegawaa@gmail.com','Satria Legawa');
            });
        }

        return back()->with('status', 1)->with('message', 'Kirim Email Berhasil');
    }
}

