<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReuniAAUController extends Controller
{
    public function index(Request $request)
    {
        return view('event.reuni_aau.index') ;
    }

    public function store(Request $request)
    {
        $user                       =   Auth::user() ;

        if ($request->key == 'ubah_kehadiran') {
            $user->ikut             =   $request->ikut;
            $user->save() ;

            if (!$request->ikut) {
                $return['status']   =   200;
                $return['msg']      =   "Update data identitas pribadi berhasil";
                return $return;
            } else {
                if ($request->ikut != 'ikut') {
                    $return['status']   =   200;
                    if ($user->ikut == 'mungkin') {
                        $return['msg']      =   "Anda all set, akan ada pemberitahuan lebih lanjut. Update kembali jika ada perubahan";
                    } else {
                        $return['msg']      =   "Besar harapan kami anda ikut serta, data kehadiran akan dihapus, jika berubah pikiran silahkan mengisi ulang data kehadiran";
                    }
                    return $return;
                } else {
                    $return['status']   =   201;
                    return $return;
                }
            }

        } else {

            //

            $user->nama_lain            =   $request->namalain ? json_encode($request->namalain) : $user->namalain ;
            $user->status_lain          =   $request->statuslain ? json_encode($request->statuslain) : $user->statuslain ;
            $user->lain_ikut            =   $request->lainikut ? json_encode($request->lainikut) : $user->lain_ikut ;

            $user->titik_penjemputan    =   $request->titik_penjemputan ;
            $user->type_kamar           =   $request->type_kamar ;

            $user->type_kamar_tambahan  =   $request->typekamar ? json_encode($request->typekamar) : $user->typekamar ;
            $user->jumlah_tambahan      =   $request->jumlahtambahan ? json_encode($request->jumlahtambahan) : $user->jumlahtambahan ;

            $user->save() ;

            $return['status']   =   200;
            return $return;
        }

    }
}
