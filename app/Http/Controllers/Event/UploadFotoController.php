<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use Illuminate\Http\Request;
use File ;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UploadFotoController extends Controller
{
    public function index(Request $request)
    {
        $data   =   Gallery::where(function($query) {
                        if (Auth::user()->status == NULL) {
                            $query->where('user_id', Auth::user()->id) ;
                        }
                    })
                    ->get() ;

        return view('gallery.index', compact('data')) ;
    }

    public function store(Request $request)
    {
        if ($request->gallery < 1) {
            return back()->with('status', 2)->with('message', 'Tidak ada yang diupload');
        }

        DB::beginTransaction() ;
        for ($x=0; $x < COUNT($request->gallery); $x++) {
            if ($request->gallery[$x]) {
                $files      =   $request->gallery[$x];
                $name_file  =   rand() . '-' . time() . '.' . $files->getClientOriginalExtension();

                $path       =   'uploads/file';
                $file       =   $path . '/' . $name_file;

                if (!File::exists($path)) {
                    File::makeDirectory($path, 0777, true);
                }

                $files->move($path, $name_file);

                $upload             =   new Gallery ;
                $upload->user_id    =   Auth::user()->id ;
                $upload->nama       =   $name_file ;
                $upload->file       =   $file ;
                if (!$upload->save()) {
                    DB::rollBack() ;
                    return back()->with('status', 2)->with('message', 'Proses Gagal') ;
                }
            }
        }
        DB::commit() ;
        return back()->with('status', 1)->with('message', 'Upload Berhasil');
    }

    public function destroy(Request $request)
    {
        $data   =   Gallery::find($request->id) ;
        File::delete($data->file) ;

        $data->delete() ;
        return back()->with('status', 1)->with('message', 'Hapus Gallery Berhasil');
    }
}
