<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        return view('home.index') ;
    }

    public function store(Request $request)
    {
        DB::beginTransaction() ;

        if (User::where('nrp', $request->password)->count() > 0) {
            return back()->with('status', 2)->with('message', 'NRP Sudah Terdaftar');
        }

        if (User::where('email', $request->email)->count() > 0) {
            return back()->with('status', 2)->with('message', 'Email Sudah Terdaftar');
        }

        if (($request->password >= 529567) && ($request->password <= 5295709)) {
            $user           =   new User ;
            $user->name     =   $request->name ;
            $user->email    =   $request->email ;
            $user->nrp      =   $request->password ;
            $user->telepon  =   $request->telepon ;
            $user->instagram=   $request->instagram ;

            $user->password =   Hash::make($user->nrp) ;

            if (!$user->save()) {
                DB::rollBack() ;
                return back()->with('status', 2)->with('message', 'Proses gagal') ;
            }

            DB::commit();
            return redirect()->route('login')->with('status', 1)->with('message', 'Anda sudah terdaftar. Silahkan lakukan login');

        } else {
            return back()->with('status', 2)->with('message', 'NRP salah');
        }


    }

    public function dashboard(Request $request)
    {
        $user   =   Auth::user() ;
        return view('home.dashboard', compact('user')) ;
    }
}
