<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfilController extends Controller
{
    public function index(Request $request)
    {
        return view('auth.profil') ;
    }

    public function store(Request $request)
    {
        if ($request->key == 'update_data') {
            $data           =   User::find($request->id) ;
            $data->name     =   $request->nama ;
            $data->nrp      =   $request->nrp ;
            $data->email    =   $request->email ;
            $data->telepon  =   $request->nomor ;
            $data->password =   Hash::make($data->nrp) ;
            $data->save() ;

            $return['status']   =   200;
            $return['msg']      =   "Ubah data berhasil";
            return $return;
        } else

        if ($request->key == 'hapus_data') {
            $data           =   User::find($request->id);

            if ($data) {
                $data->email    =   "dihapus_" . $data->email ;
                $data->nrp      =   "99" . $data->nrp ;
                $data->save() ;
                $data->delete() ;

                $return['status']   =   200;
                $return['msg']      =   "Hapus data berhasil";
                return $return;
            }

            $return['status']   =   400;
            $return['msg']      =   "Proses Gagal";
            return $return;
        } else {

            if (!$request->tanggal_lahir) {
                $return['status']   =   400 ;
                $return['msg']      =   "Tanggal Lahir Wajib Diisikan" ;
                return $return ;
            }

            if (!$request->pangkat) {
                $return['status']   =   400 ;
                $return['msg']      =   "Pangkat Wajib Diisikan" ;
                return $return ;
            }

            if (!$request->korp) {
                $return['status']   =   400 ;
                $return['msg']      =   "Korp Wajib Diisikan" ;
                return $return ;
            }

            if (!$request->jabatan) {
                $return['status']   =   400 ;
                $return['msg']      =   "Jabatan Wajib Diisikan" ;
                return $return ;
            }

            if (!$request->kesatuan) {
                $return['status']   =   400 ;
                $return['msg']      =   "Kesatuan Wajib Diisikan" ;
                return $return ;
            }

            if (!$request->alamat_rumah) {
                $return['status']   =   400 ;
                $return['msg']      =   "Alamat Surat Menyurat Wajib Diisikan" ;
                return $return ;
            }

            if (!$request->asal_daerah) {
                $return['status']   =   400 ;
                $return['msg']      =   "Asal Daerah Wajib Dipilih" ;
                return $return ;
            }

            if (!$request->jabatan_taruna) {
                $return['status']   =   400 ;
                $return['msg']      =   "Jabatan di Taruna Wajib Diisikan" ;
                return $return ;
            }

            if (!$request->kamar_tingkat3) {
                $return['status']   =   400 ;
                $return['msg']      =   "No. Kamar Tingkat 3 Wajib Diisikan" ;
                return $return ;
            }

            if (!$request->kopel) {
                $return['status']   =   400 ;
                $return['msg']      =   "Kopel Wajib Diisikan" ;
                return $return ;
            }

            if (!$request->check_poin_pesiar) {
                $return['status']   =   400 ;
                $return['msg']      =   "Check Poin Pesiar Wajib Diisikan" ;
                return $return ;
            }

            if (!$request->ukuran_kaos_peserta) {
                $return['status']   =   400 ;
                $return['msg']      =   "Ukuran Kaos Wajib Diisikan" ;
                return $return ;
            }

            if (!$request->ikut) {
                $return['status']   =   400 ;
                $return['msg']      =   "Konfirmasi Kehadiran Wajib Dipilih" ;
                return $return ;
            }


            $user                       =   Auth::user() ;
            $user->instagram            =   $request->instagram ;
            $user->tanggal_lahir        =   $request->tanggal_lahir ;
            $user->pangkat              =   $request->pangkat ;
            $user->korp                 =   $request->korp ;
            $user->jabatan              =   $request->jabatan ;
            $user->kesatuan             =   $request->kesatuan ;
            $user->alamat_rumah         =   $request->alamat_rumah ;
            $user->asal_daerah          =   $request->asal_daerah ;
            $user->jabatan_taruna       =   $request->jabatan_taruna ;
            $user->kamar_tingkat3       =   $request->kamar_tingkat3 ;
            $user->kopel                =   $request->kopel ;
            $user->ukuran_kaos_peserta  =   $request->ukuran_kaos_peserta ;
            $user->check_poin_pesiar    =   $request->check_poin_pesiar ;
            $user->istri                =   $request->istri ;
            $user->ukuran_kaos_istri    =   $request->ukuran_kaos_istri ;
            $user->istri_hadir          =   $request->istri_hadir ;
            $user->anak                 =   $request->anak ;
            $user->anak_ikut            =   $request->anakikut ;
            $user->umur                 =   $request->umur ;
            $user->ikut                 =   $request->ikut ;
            $user->save() ;

            if (!$request->ikut) {
                $return['status']   =   200;
                $return['msg']      =   "Update data identitas pribadi berhasil";
                return $return;
            } else {
                if ($request->ikut == 'tidak') {
                    $return['status']   =   200 ;
                    $return['msg']      =   "Besar harapan kami anda ikut serta, data kehadiran akan dihapus, jika berubah pikiran silahkan mengisi ulang data kehadiran";
                    return $return ;
                } else {
                    $return['status']   =   201;
                    return $return;
                }
            }
        }
    }
}
