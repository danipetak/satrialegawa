<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->integer('nrp')->nullable();
            $table->string('telepon', 25)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->date('tanggal_lahir')->nullable();
            $table->string('instagram', 100)->nullable();
            $table->string('pangkat', 100)->nullable();
            $table->string('jabatan', 100)->nullable();
            $table->string('kesatuan', 100)->nullable();
            $table->string('alamat_rumah', 100)->nullable();
            $table->string('asal_daerah', 100)->nullable();
            $table->string('jabatan_taruna', 100)->nullable();
            $table->string('kamar_tingkat3', 100)->nullable();
            $table->string('kopel', 100)->nullable();
            $table->string('check_poin_pesiar', 100)->nullable();
            $table->string('ukuran_kaos_peserta', 10)->nullable();
            $table->string('istri', 100)->nullable();
            $table->string('ukuran_kaos_istri', 10)->nullable();
            $table->text('anak')->nullable();
            $table->text('umur')->nullable();
            $table->string('ikut', 10)->nullable();
            $table->text('nama_lain')->nullable();
            $table->text('status_lain')->nullable();
            $table->string('titik_penjemputan', 100)->nullable();
            $table->string('type_kamar', 100)->nullable();
            $table->text('type_kamar_tambahan')->nullable();
            $table->text('jumlah_tambahan')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
